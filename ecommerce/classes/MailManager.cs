﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace ecommerce.classes
{
    public class MailManager
    {

        public static bool SendMail(string Key,

                              Dictionary<string, string> to,
                              string subject,
                              string MailBody,
                              List<string> from_name,
                              List<string> attachment)
        {
            try
            {
                Api test = new Api(Key);

                Dictionary<string, Object> data = new Dictionary<string, Object>();
                //to.Add("shahzadq58@gmail.com", "shahzadq58@gmail.com");
                // List<string> from_name = new List<string>();
                // to.Add("online@clickUrtrip.com", "Clickurtrip");
                // from_name.Add("from email!");
                //List<string> attachment = new List<string>();
                // attachment.Add("https://clickurtrip.com/images/Logo.png");
                //attachment.Add("https://domain.com/path-to-file/filename2.jpg");

                data.Add("to", to);
                // data.Add("cc", to);
                data.Add("from", from_name);
                data.Add("subject", subject);
                data.Add("html", MailBody);
                if (attachment != null && attachment.Count != 0)
                    data.Add("attachment", attachment);
                Object sendEmail = test.send_email(data);
                //return sendEmail.ToString();
                return true;
            }
            catch
            {
                return false;
            }

        }

        public static bool SendMail(string Key,

                             Dictionary<string, string> to,
                             Dictionary<string, string> Bcc,
                             string subject,
                             string MailBody,
                             List<string> from_name,
                             List<string> attachment)
        {
            try
            {
                Api test = new Api(Key);
                Dictionary<string, Object> data = new Dictionary<string, Object>();
                data.Add("to", to);
                // data.Add("bcc", to);
                data.Add("cc", Bcc);
                data.Add("from", from_name);
                data.Add("subject", subject);
                data.Add("html", MailBody);
                if (attachment != null && attachment.Count != 0)
                    data.Add("attachment", attachment);
                Object sendEmail = test.send_email(data);
                //return sendEmail.ToString();
                return true;
            }
            catch
            {
                return false;
            }

        }



        public static string GetTemplate()
        {
            Api test = new Api(ConfigurationManager.AppSettings["AccessKey"]);
            Dictionary<string, Object> data = new Dictionary<string, Object>();
            data.Add("type", "template");
            data.Add("status", "temp_inactive");
            data.Add("page", 1);
            data.Add("page_limit", 10);
            Object getCampaigns = test.get_campaigns_v2(data);
            var Response = getCampaigns.ToString();

            return Response;
        }


        public static bool SendMailTemplates(Int64 ID, Dictionary<string, string> attribute, Dictionary<string, string> to,
                                             Dictionary<string, string> Bcc, string subject, string MailBody,
                                             List<string> from_name, List<string> attachment)
        {
            try
            {
                Api test = new Api(ConfigurationManager.AppSettings["AccessKey"]);
                Dictionary<string, Object> data = new Dictionary<string, Object>();
                //Dictionary<string, string> attribute = new Dictionary<string, string>();
                //attribute.Add("TO", "Kashif Khan");
                //attribute.Add("FROM", "http://clickurtrip.com//images//Logo.png");
                //Dictionary<string, string> attachment = new Dictionary<string, string>();
                //attachment.Add("myfilename.pdf", "your_pdf_files_base64_encoded_chunk_data");
                Dictionary<string, string> headers = new Dictionary<string, string>();
                headers.Add("Content-Type", "text/html; charset=iso-8859-1");
                headers.Add("X-param1", "value1");
                headers.Add("X-param2", "value2");
                headers.Add("X-Mailin-custom", "my custom value");
                headers.Add("X-Mailin-Tag", "My tag");

                data.Add("id", ID);
                data.Add("to", to);
                data.Add("cc", Bcc);
                // data.Add("bcc", "bcc-example@example.net");
                // data.Add("replyto", "support@clickurtrip.com");
                data.Add("attr", attribute);
                // data.Add("attachment_url", "");
                // data.Add("attachment", attachment);
                data.Add("headers", headers);

                Object sendTransactionalTemplate = test.send_transactional_template(data);
                Console.WriteLine(sendTransactionalTemplate);

                return true;
            }
            catch
            {
                return false;
            }
        }

    }
}