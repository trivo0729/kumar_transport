﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Net;
using System.Configuration;

namespace ecommerce.classes
{
    public class TemplateGenrator
    {
        static readonly Regex re = new Regex(@"\$(\w+)\$", RegexOptions.Compiled);
        public class Token
        {
            public string TokenValue { get; set; }
            public string SampleValue { get; set; }
        }
        public static List<Token> GetTokenList()
        {
            List<Token> tokens = new List<Token>();
            foreach (var Parms in arrParms)
            {
                tokens.Add(new Token() { TokenValue = Parms.Key, SampleValue = Parms.Value });
            }
            return tokens;
        }
        public static string Path { get; set; }
        public static string GetTemplate
        {
            get
            {

                Path = HttpContext.Current.Server.MapPath("~/" + Path);
                string sTemplate = "";
                using (var reader = new StreamReader(Path))
                {
                    while (!reader.EndOfStream)
                    {
                        sTemplate += reader.ReadLine();
                    }
                }
                //sTemplate =re.Replace(sTemplate, match => arrParms[match.Groups[1].Value]);
                sTemplate = arrParms.Aggregate(sTemplate, (current, value) =>
                               current.Replace(value.Key.Replace("$", ""), value.Value));
                return sTemplate.ToString();
            }
        }
        public static Dictionary<string, string> arrParms { get; set; }

        public static bool Post(string data, out string response)
        {
            response = "";
            try
            {
                var oRequest = WebRequest.Create(data);
                oRequest.Method = "GET";
                oRequest.Timeout = 300000;
                using (var oResponse = oRequest.GetResponse())
                {
                    using (var oResponseStream = oResponse.GetResponseStream())
                    {
                        System.IO.StreamReader sr = new System.IO.StreamReader(oResponseStream);
                        response = sr.ReadToEnd();
                        if (response.Contains("error"))
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                }
            }
            catch (WebException oWebException)
            {
                if (oWebException.Response != null)
                {
                    using (var oResponseStream = oWebException.Response.GetResponseStream())
                    {
                        System.IO.StreamReader sr = new System.IO.StreamReader(oResponseStream);
                        response = sr.ReadToEnd();
                    }
                }
                return false;
            }
            catch (Exception oException)
            {
                response = oException.Message;
                return false;
            }
        }

        public static string GetTemplatePath(string TemplateName)
        {
            string sTemplate = string.Empty;
            try
            {
                string pathDownload = GetAppServerFolder;
                DirectoryInfo directory = new DirectoryInfo(pathDownload);
                DirectoryInfo[] directories = directory.GetDirectories();
                foreach (DirectoryInfo folder in directories)
                {
                    string Folder = folder.Name;
                    if (TemplateName == Folder)
                    {
                        List<FileInfo> files = folder.GetFiles().ToList();
                        Path = GetAppRootFolder + "/" + Folder + "/" + files[0].Name;
                    }
                }
            }

            catch (Exception ex)
            {
                //throw;
            }
            return sTemplate;
        }

        #region Email Template
        public static string GetAppServerFolder
        {
            get
            {
                return HttpContext.Current.Server.MapPath("~/" + ConfigurationManager.AppSettings["EmailRootFolder"] + "/");
            }
        }
        public static string GetAppRootFolder
        {
            get
            {
                return ConfigurationManager.AppSettings["EmailRootFolder"];
            }
        }

        public static string GetAssignTemplate(Int64 Admin, string Template)
        {
            try
            {
                return "";
            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion


        //public static List<string> GetSubMenu(string Folder)
        //{
        //    List<string> ListMenu = new List<string>();
        //    try
        //    {
        //        string pathDownload = GetAppServerFolder + "\\" + Folder;
        //        DirectoryInfo directory = new DirectoryInfo(pathDownload);
        //        DirectoryInfo[] directories = directory.GetDirectories();
        //        foreach (DirectoryInfo folder in directories)
        //        {
        //            CutAdmin.Models.MenuItem arrMenu = new Models.MenuItem();
        //            arrMenu = new CutAdmin.Models.MenuItem { Name = folder.Name.Split('.')[0], ChildItem = new List<CutAdmin.Models.MenuItem>(), Path = string.Empty };
        //            List<FileInfo> files = new List<FileInfo>();
        //            files = folder.GetFiles().ToList();
        //            if (files.Count != 0)
        //            {
        //                if (files.Count == 1)
        //                    arrMenu.Path = GetAppRootFolder + "/" + Folder + "/" + folder + "/" + files[0].Name;
        //                else
        //                    foreach (var File in files)
        //                    {
        //                        arrMenu.ChildItem.Add(new Models.MenuItem
        //                        {
        //                            Name = File.Name.Split('.')[0],
        //                            ChildItem = new List<CutAdmin.Models.MenuItem>(),
        //                            Path = GetAppRootFolder + "/" + Folder + "/" + folder + "/" + File.Name,
        //                        });
        //                    }
        //            }
        //            ListMenu.Add(arrMenu);
        //        }
        //    }
        //    catch (Exception)
        //    {
        //    }
        //    return ListMenu;
        //}

        //public static string GetTemplatePath(Int64 AdminID, string TemplateName)
        //{
        //    string sTemplate = string.Empty;
        //    try
        //    {
        //        using (var DB = new Trivo_AmsHelper())
        //        {
        //            var List = (from obj in DB.tbl_Admin where obj.ID == AdminID select obj).FirstOrDefault();
        //            var ListName = (from obj in DB.tbl_EmailTemplates where obj.nAdminID == AdminID && obj.sTemplateName == TemplateName select obj).FirstOrDefault();
        //            string ServerPath = "";
        //            if(ListName != null)
        //            {
        //                Path = ListName.sPath;

        //            }
        //            else
        //            {
        //                string pathDownload = Common.Common.GetAppServerFolder;
        //                DirectoryInfo directory = new DirectoryInfo(pathDownload);
        //                DirectoryInfo[] directories = directory.GetDirectories();
        //                foreach (DirectoryInfo folder in directories)
        //                {
        //                    string Folder = folder.Name;
        //                    var arrList = EmailTemplate.GetSubMenu(folder.Name);
        //                    foreach (var Menu in arrList)
        //                    {
        //                        if(TemplateName == Menu.Name)
        //                        {
        //                            if(Menu.ChildItem.Count ==0)
        //                            {
        //                                Path = Menu.Path;
        //                            }
        //                            else
        //                            {
        //                                Path = Menu.ChildItem.FirstOrDefault().Path;
        //                            }
        //                            break;
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }

        //    catch (Exception ex)
        //    {
        //        //throw;
        //    }
        //    return sTemplate;
        //}
    }
}