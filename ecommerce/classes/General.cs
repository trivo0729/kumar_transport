﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using ecommerce.modal;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Newtonsoft.Json;

namespace ecommerce.classes
{
    public class General
    {

        public static string LoginUser(string UserName, string Password)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            string json = "";
            try
            {
                using (var db = new royalbor_hairEntities())
                {
                    var user = (from u in db.tbl_CustomerLogin where u.Email == UserName && u.Password == Password select u).FirstOrDefault();
                    if (user != null)
                    {
                        HttpCookie userInfo = new HttpCookie("userInfo");
                        userInfo["UserId"] = user.nID.ToString();
                        userInfo.Expires = DateTime.Now.AddDays(365);
                        HttpContext.Current.Response.Cookies.Add(userInfo);
                        json = GetUserData();
                    }

                    else
                        json = jsSerializer.Serialize(new { retCode = 0, message = "user name or password is incorrect" });
                }
            }
            catch (Exception ex)
            {

                json = jsSerializer.Serialize(new { retCode = 0, message = ex.Message });
            }
            return json;
        }

        public static string logout()
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            string json = "";
            try
            {
                //Fetch the Cookie using its Key.
                HttpCookie userInfo = HttpContext.Current.Request.Cookies["userInfo"];
                if (userInfo != null)
                {
                    //Set the Expiry date to past date.
                    userInfo.Expires = DateTime.Now.AddDays(-1);
                    //Update the Cookie in Browser.
                    HttpContext.Current.Response.Cookies.Add(userInfo);
                    json = jsSerializer.Serialize(new { retCode = 1, message = "user logged out successfully" });
                }
                else
                {
                    json = jsSerializer.Serialize(new { retCode = 0, message = "user not logged in" });
                }
            }
            catch (Exception ex)
            {
                json = jsSerializer.Serialize(new { retCode = 0, message = ex.Message });
            }
            return json;
        }

        public static string GetUserData()
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            string json = "";
            HttpCookie Cookiesdata = HttpContext.Current.Request.Cookies["userInfo"];
            if (Cookiesdata != null)
            {
                try
                {
                    string UserId = Cookiesdata["UserId"].ToString();
                    using (var db = new royalbor_hairEntities())
                    {
                        var user = (from u in db.tbl_CustomerLogin
                                    where u.nID.ToString() == UserId
                                    select new
                                    {
                                        UserId = u.nID,
                                        Email = u.Email,
                                        FirstName = u.First_Name,
                                        LastName = u.Last_Name,
                                        Address = u.Address,
                                        Phone = u.Phone,
                                        PostCode = u.PostCode,
                                        Country = u.Country,
                                        State = u.State,
                                        City = u.City,
                                    }).FirstOrDefault();
                        if (user != null)
                        {
                            json = jsSerializer.Serialize(new { retCode = 1, userdetails = user });
                        }
                    }
                }
                catch (Exception ex)
                {

                    json = jsSerializer.Serialize(new { retCode = 0, message = ex.Message });
                }
            }
            else
            {
                json = jsSerializer.Serialize(new { retCode = 0, message = "user not logged in" });
            }
            return json;
        }


        public static string Registeruser(string FirstName, string LastName, string Email, string Mobile, string Country, string State, string City, string Address, string Zipcode, string Password)
        {
            string json = "";
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            try
            {

                using (var db = new royalbor_hairEntities())
                {
                    var email = (from u in db.tbl_CustomerLogin where u.Email == Email select u.Email).FirstOrDefault();
                    if (email == null)
                    {

                        var user = new tbl_CustomerLogin()
                        {
                            First_Name = FirstName,
                            Last_Name = LastName,
                            Email = Email,
                            Phone = Mobile,
                            Country = Country,
                            City = City,
                            State = State,
                            Address = Address,
                            PostCode = Zipcode,
                            Password = Password
                        };
                        db.tbl_CustomerLogin.Add(user);
                        db.SaveChanges();

                        HttpCookie userInfo = new HttpCookie("userInfo");
                        userInfo["UserId"] = user.nID.ToString();
                        userInfo.Expires = DateTime.Now.AddDays(365);
                        HttpContext.Current.Response.Cookies.Add(userInfo);
                        bool sentmail = RegistrationMail(Email, Password);
                        if (sentmail == true)
                        {
                            json = GetUserData();
                        }
                        else
                        {
                            json = jsSerializer.Serialize(new { retCode = 0, message = "something went wrong" });
                        }

                    }
                    else
                    {
                        json = jsSerializer.Serialize(new { retCode = 0, message = "entered email already exists please try another email" });
                    }

                }
            }
            catch (Exception ex)
            {

                json = jsSerializer.Serialize(new { retCode = 0, message = ex.Message });
            }
            return json;
        }

        #region Registration Mail To Customer
        public static bool RegistrationMail(string Email, string Password)
        {
            try
            {
                TemplateGenrator.GetTemplatePath("Registration");
                String Mailhtml = "";
                TemplateGenrator.arrParms = new Dictionary<string, string> {
                         {"_UserName",Email },
                         {"_Password",Password},
                         {"_CustomerEmail",Email}
                         };
                Mailhtml += TemplateGenrator.GetTemplate;
                List<string> from = new List<string>();
                from.Add(Convert.ToString(ConfigurationManager.AppSettings["supportMail"]));
                List<string> DocLinksList = new List<string>();
                Dictionary<string, string> Email1List = new Dictionary<string, string>();

                foreach (string mail in Email.Split(','))
                {
                    if (mail != "")
                    {
                        Email1List.Add(mail, mail);
                    }
                }
                string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);
                bool reponse = MailManager.SendMail(accessKey, Email1List, "Your Wogo Naturals login Details ", Mailhtml, from, DocLinksList);
                return reponse;
            }
            catch (Exception ex)
            {

                return false;
            }
        }
        #endregion

        #region Forgot Mail 
        public static string ForgetPass(string email)
        {
            using (var db = new royalbor_hairEntities())
            {
                string json = "";
                try
                {
                    var List = (from user in db.tbl_CustomerLogin where user.Email == email select user).FirstOrDefault();
                    if (List != null)
                    {
                        var Password = List.Password;
                        ForgotMail(email, Password);
                        json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                    }
                    else
                        json = "{\"Session\":\"1\",\"retCode\":\"2\"}";

                }
                catch (Exception ex)
                {

                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
                return json;
            }
        }


        public static bool ForgotMail(string Email, string Password)
        {
            try
            {
                TemplateGenrator.GetTemplatePath("Registration");
                String Mailhtml = "";
                TemplateGenrator.arrParms = new Dictionary<string, string> {
                         {"_UserName",Email },
                         {"_Password",Password},
                         {"_CustomerEmail",Email}
                         };
                Mailhtml += TemplateGenrator.GetTemplate;
                List<string> from = new List<string>();
                from.Add(Convert.ToString(ConfigurationManager.AppSettings["supportMail"]));
                List<string> DocLinksList = new List<string>();
                Dictionary<string, string> Email1List = new Dictionary<string, string>();

                foreach (string mail in Email.Split(','))
                {
                    if (mail != "")
                    {
                        Email1List.Add(mail, mail);
                    }
                }
                string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);
                bool reponse = MailManager.SendMail(accessKey, Email1List, "Your Wogo Naturals login Details ", Mailhtml, from, DocLinksList);
                return reponse;
            }
            catch (Exception ex)
            {

                return false;
            }
        }
        #endregion

        #region Contact Mail

        public static string SendMail(string userName, string nMobile, string email, string subject, string message)
        {
            //string html = emailmanager.SendMail(userName, nMobile, email, subject, message);
            string json = "";
            try
            {

                ContactMail(userName, nMobile, email, subject, message);
                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }
        public static bool ContactMail(string userName, string nMobile, string email, string subject, string message)
        {
            try
            {


                TemplateGenrator.GetTemplatePath("Enquiry");
                string Mailhtml = "";
                TemplateGenrator.arrParms = new Dictionary<string, string> {
                         {"_AEnquiryDate",DateTime.Now.ToString("dd-MM-yyy") },
                         {"_UserName", userName},
                         {"_Usernumber", nMobile},
                         {"_UserEmailID", email},
                         {"_UserSubject", subject},
                         {"_UserMessage", message},
                        // {"_ShippingAdd", ""+ Address +" <br>"+ City +" , "+ State +"<br>"+ Country +", "+ PostCode +""},
                        // {"_AItems", Producthtml}
                         };
                Mailhtml += TemplateGenrator.GetTemplate;

                List<string> from = new List<string>();
                from.Add(Convert.ToString(ConfigurationManager.AppSettings["OrderReceiveMail"]));
                List<string> DocLinksList = new List<string>();
                Dictionary<string, string> Email1List = new Dictionary<string, string>();
                var sellermail = Convert.ToString(ConfigurationManager.AppSettings["OrderReceiveMail"]);
                foreach (string mail in sellermail.Split(','))
                {
                    if (mail != "")
                    {
                        Email1List.Add(mail, mail);
                    }
                }
                string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);
                bool reponse = MailManager.SendMail(accessKey, Email1List, "Customer Enquiry Details", Mailhtml, from, DocLinksList);
                return reponse;
            }
            catch (Exception ex)
            {

                return false;
            }
        }
        #endregion
        public static string BookProduct(string FirstName, string LastName, string Email, string Address, string Phone, string City, string PostCode, string Country, string State, string InvoiceNo, string subTotal, string amount, string couponid)
        {
            string json = "";
            string responseXML = "";
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            HttpCookie Cookiesdata = HttpContext.Current.Request.Cookies["userInfo"];
            if (Cookiesdata != null)
            {
                try
                {
                    string UserId = Cookiesdata["UserId"].ToString();

                    using (var db = new royalbor_hairEntities())
                    {
                        var user = db.tbl_CustomerLogin.FirstOrDefault(b => b.nID.ToString() == UserId);
                        if (user != null)
                        {
                            user.First_Name = FirstName;
                            user.Last_Name = LastName;
                            user.Phone = Phone;
                            user.Country = Country;
                            user.State = State;
                            user.City = City;
                            user.PostCode = PostCode;
                            user.Address = Address;
                            db.SaveChanges();
                        }

                        string InvoiceDate = DateTime.Now.ToString("dd-MM-yyy");
                        string PaymentType = "";
                        Decimal VAT = 0;
                        string Sub = subTotal;
                        Decimal SubTotal = Convert.ToDecimal(Sub);
                        string Tot = amount;
                        Decimal Total = Convert.ToDecimal(Tot);

                        AddToCart objAddToCart = new AddToCart();
                        JavaScriptSerializer jsSerailize = new JavaScriptSerializer();
                        objAddToCart = (AddToCart)HttpContext.Current.Session["AddToCarts"];

                        decimal PriceTotal = 0;
                        foreach (listItem objlistItem in objAddToCart.ItemList)
                        {
                            PriceTotal = Convert.ToDecimal(objlistItem.quantity * objlistItem.price);

                            var PID = objlistItem.productCode;
                            var PName = objlistItem.productName;
                            var PQnty = objlistItem.quantity;
                            var PPrice = objlistItem.price;

                            var online = new tbl_OnlineParticular()
                            {
                                InvoiceNo = InvoiceNo,
                                InvoiceDate = DateTime.Now.ToString("dd-MM-yyy"),
                                Total = PriceTotal,
                                Product_ID = Convert.ToInt64(PID),
                                Quantity = PQnty,
                                Product_Price = PPrice,
                                Customer_nID = Convert.ToInt64(UserId),
                                Product_Name = PName
                            };
                            db.tbl_OnlineParticular.Add(online);
                            db.SaveChanges();
                        }

                        var billing = new tbl_Checkout()
                        {
                            Invoice_No = InvoiceNo,
                            Invoice_Date = DateTime.Now.ToString("dd-MM-yyy"),
                            First_Name = FirstName,
                            Last_Name = LastName,
                            Email = Email,
                            Address = Address,
                            Phone = Phone,
                            Country = Country,
                            State = State,
                            City = City,
                            PostCode = PostCode,
                        };
                        db.tbl_Checkout.Add(billing);
                        db.SaveChanges();

                        var Sale = new tbl_OnlineSales()
                        {
                            Customer_ID = Convert.ToInt64(UserId),
                            InvoiceNo = InvoiceNo,
                            InvoiceDate = InvoiceDate,
                            Payment_Type = PaymentType,
                            Sub_Total = SubTotal,
                            Vat_Amount = VAT,
                            Grand_Total = Total,
                            VAT_Percent = 0,
                            Status = "Requested",
                            DeliveryBy = "Admin",
                            Delivery_Date = DateTime.Now.AddDays(7).ToString(),
                            Offer_nID = couponid
                        };
                        db.tbl_OnlineSales.Add(Sale);
                        db.SaveChanges();
                        bool sentToCustomer = BookingMail(Email, InvoiceNo);
                        bool sentToSeller = SellerBookingMail(Email, InvoiceNo, Address, Phone, Country, State, City, PostCode, FirstName, LastName);
                     
                        objAddToCart = new AddToCart();
                        objAddToCart = (AddToCart)HttpContext.Current.Session["AddToCarts"];
                        if (objAddToCart != null)
                        {
                            objAddToCart = null;
                            HttpContext.Current.Session["AddToCarts"] = objAddToCart;
                            json = getCartItem();
                        }

                        return json = jsSerializer.Serialize(new { retCode = 1 });
                    }

                }
                catch (Exception ex)
                {

                    json = jsSerializer.Serialize(new { retCode = 0, message = ex.Message });
                }
            }
            return json;
        }
        public static string AddToCart(string Name, decimal SellingPrice, string Image, string Code, Int64 Qnty)
        {
            string json = "";
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            try
            {
                AddToCart Cart = new AddToCart();
                if (HttpContext.Current.Session["AddToCarts"] == null)
                {
                    List<listItem> listItem = new List<listItem>();
                    listItem objlistItem = new listItem();
                    objlistItem.productName = Name;
                    objlistItem.quantity = Qnty;
                    objlistItem.price = SellingPrice;
                    objlistItem.imageUrl = Image;
                    objlistItem.productCode = Code;
                    listItem.Add(objlistItem);
                    Cart.ItemList = listItem;
                    HttpContext.Current.Session["AddToCarts"] = Cart;
                }
                else
                {
                    Cart = new AddToCart();
                    Cart = (AddToCart)HttpContext.Current.Session["AddToCarts"];
                    if (Cart.ItemList.Exists(data => data.productCode == Code))
                    {
                        Cart.ItemList.Where(w => w.productCode == Code).Select(w => w.quantity = w.quantity + Qnty).ToList();
                        HttpContext.Current.Session["AddToCarts"] = Cart;
                    }

                    else
                    {
                        List<listItem> listItem = Cart.ItemList;
                        listItem objlistItem = new listItem();
                        objlistItem.productName = Name;
                        objlistItem.quantity = Qnty;
                        objlistItem.price = SellingPrice;
                        objlistItem.imageUrl = Image;
                        objlistItem.productCode = Code;
                        listItem.Add(objlistItem);
                        Cart.ItemList = listItem;
                        HttpContext.Current.Session["AddToCarts"] = Cart;
                    }
                }
                json = getCartItem();
            }
            catch (Exception ex)
            {

                json = jsSerializer.Serialize(new { retCode = 0, message = ex.Message });
            }
            return json;
        }

        public static string getCartItem()
        {
            string json = "";
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            try
            {
                AddToCart objAddToCart = new AddToCart();
                objAddToCart = (AddToCart)HttpContext.Current.Session["AddToCarts"];
                if (objAddToCart != null)
                {
                    float GrndTotal = 0;
                    foreach (listItem Item in objAddToCart.ItemList)
                    {
                        GrndTotal += Convert.ToSingle(Item.quantity * Item.price);
                        Item.subTotal = Convert.ToDecimal(Item.quantity * Item.price);
                    }
                    objAddToCart.ItemCount = objAddToCart.ItemList.Count();
                    objAddToCart.SubTotal = GrndTotal.ToString();
                    HttpContext.Current.Session["AddToCarts"] = objAddToCart;
                    json = jsSerializer.Serialize(new { retCode = 1, listItem = (AddToCart)HttpContext.Current.Session["AddToCarts"], GrndTotal = GrndTotal });
                }
                else
                {
                    json = jsSerializer.Serialize(new { retCode = 0, message = "product not available" });
                }

            }
            catch (Exception ex)
            {
                json = jsSerializer.Serialize(new { retCode = 0, message = ex.Message });
            }
            return json;
        }

        #region Booking Mail To Customer
        public static bool BookingMail(string Email, string OrderId)
        {
            try
            {
                AddToCart objAddToCart = new AddToCart();
                objAddToCart = (AddToCart)HttpContext.Current.Session["AddToCarts"];
                string Product_html = "";
                float GrndTotal = 0;
                decimal DiscountPrice = 0;
                DiscountPrice = Convert.ToDecimal(objAddToCart.OfferValue);
                string TotalPrice = objAddToCart.SubTotal;

                if (objAddToCart != null)
                {
                    foreach (listItem Item in objAddToCart.ItemList)
                    {
                        GrndTotal += Convert.ToSingle(Item.quantity * Item.price);
                        float totalPrice = Convert.ToSingle(Item.quantity * Item.price);
                        TemplateGenrator.GetTemplatePath("Product");
                        TemplateGenrator.arrParms = new Dictionary<string, string> {
                         {"_Count",Item.quantity.ToString() },
                         {"_ProductName",Item.productName },
                         {"_Price", totalPrice.ToString()},

                         };
                        Product_html += TemplateGenrator.GetTemplate;
                    }
                }
                TemplateGenrator.GetTemplatePath("Confirm");
                String sMail = "";
                TemplateGenrator.arrParms = new Dictionary<string, string> {
                         {"_BookingDate",DateTime.Now.ToString("dd-MM-yyy") },
                         {"_Total",GrndTotal.ToString() },
                         {"_Disount",DiscountPrice.ToString() },
                           {"_GrandTotal",TotalPrice},
                         {"_OrderId", OrderId},
                         {"_Items", Product_html}
                         };
                sMail += TemplateGenrator.GetTemplate;

                List<string> from = new List<string>();
                from.Add(Convert.ToString(ConfigurationManager.AppSettings["supportMail"]));
                List<string> DocLinksList = new List<string>();
                Dictionary<string, string> Email1List = new Dictionary<string, string>();

                foreach (string mail in Email.Split(','))
                {
                    if (mail != "")
                    {
                        Email1List.Add(mail, mail);
                    }
                }
                string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);

                bool reponse = MailManager.SendMail(accessKey, Email1List, "Your Booking Detail", sMail, from, DocLinksList);
                return reponse;
            }
            catch (Exception ex)
            {

                return false;
            }
        }
        #endregion

        #region Booking Mail To Seller
        public static bool SellerBookingMail(string Email, string OrderId, string Address, string Phone, string Country, string State, string City, string PostCode, string FirstName, string LastName)
        {
            try
            {
                AddToCart objAddToCart = new AddToCart();
                objAddToCart = (AddToCart)HttpContext.Current.Session["AddToCarts"];
                string Producthtml = "";
                float GrndTotal = 0;
                //For Discount Price
                Decimal Discountprice = 0;
                Discountprice = objAddToCart.OfferValue;
                string ActualTotal = objAddToCart.SubTotal;
                if (objAddToCart != null)
                {
                    foreach (listItem Item in objAddToCart.ItemList)
                    {
                        GrndTotal += Convert.ToSingle(Item.quantity * Item.price);
                        float totalprice = Convert.ToSingle(Item.quantity * Item.price);
                        TemplateGenrator.GetTemplatePath("AProducts");
                        TemplateGenrator.arrParms = new Dictionary<string, string> {
                         {"_ACount",Item.quantity.ToString() },
                         {"_AProductName",Item.productName },
                         {"_APrice", totalprice.ToString()},

                         };
                        Producthtml += TemplateGenrator.GetTemplate;
                    }
                }
                TemplateGenrator.GetTemplatePath("Admin");
                string Mailhtml = "";
                TemplateGenrator.arrParms = new Dictionary<string, string> {
                         {"_ABookingDate",DateTime.Now.ToString("dd-MM-yyy") },
                         {"_ATotal",GrndTotal.ToString() },
                         {"_DiscountValue",Discountprice.ToString() },
                         {"_AGrandtotal",ActualTotal },
                         {"_AOrderId", OrderId},
                         {"_CustomerName", FirstName + " " + LastName},
                         {"_Customernumber", Phone},
                         {"_CustomerEmailID", Email},
                         {"_ShippingAdd", ""+ Address +" <br>"+ City +" , "+ State +"<br>"+ Country +", "+ PostCode +""},
                         {"_AItems", Producthtml}
                         };
                Mailhtml += TemplateGenrator.GetTemplate;

                List<string> from = new List<string>();
                from.Add(Convert.ToString(ConfigurationManager.AppSettings["supportMail"]));
                List<string> DocLinksList = new List<string>();
                Dictionary<string, string> Email1List = new Dictionary<string, string>();
                var sellermail = Convert.ToString(ConfigurationManager.AppSettings["OrderReceiveMail"]);
                foreach (string mail in sellermail.Split(','))
                {
                    if (mail != "")
                    {
                        Email1List.Add(mail, mail);
                    }
                }
                string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);
                bool reponse = MailManager.SendMail(accessKey, Email1List, "Your Booking Detail", Mailhtml, from, DocLinksList);
                return reponse;
            }
            catch (Exception ex)
            {

                return false;
            }
        }
        #endregion
        public static string RemoveItem(string productCode)
        {
            string json = "";
            AddToCart objAddToCart = new AddToCart();
            objAddToCart = new AddToCart();
            objAddToCart = (AddToCart)HttpContext.Current.Session["AddToCarts"];
            if (objAddToCart.ItemList.Exists(data => data.productCode == productCode))
            {
                objAddToCart.ItemList.RemoveAll((x) => x.productCode == productCode);
                HttpContext.Current.Session["AddToCarts"] = objAddToCart;
                json = getCartItem();
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

     

        public static string getorders()
        {
            string json = "";
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            HttpCookie Cookiesdata = HttpContext.Current.Request.Cookies["userInfo"];
            if (Cookiesdata != null)
            {
                string UserId = Cookiesdata["UserId"].ToString();
                try
                {
                    using (var db = new royalbor_hairEntities())
                    {
                        List<Orders> Order = new List<Orders>();
                        var Orderslist = (from sale in db.tbl_OnlineSales
                                          where sale.Customer_ID.ToString() == UserId
                                          orderby sale.nID descending
                                          select new
                                          {
                                              sale.InvoiceNo,
                                              sale.InvoiceDate,
                                              sale.Status,
                                              sale.Grand_Total,
                                          }).ToList();
                        if (Orderslist.Count() != 0)
                        {
                            foreach (var item in Orderslist.ToList())
                            {
                                IQueryable<Products> PerOrders = (from product in db.tbl_OnlineParticular
                                                                  where product.Customer_nID.ToString() == UserId && item.InvoiceNo == product.InvoiceNo
                                                                  select new Products
                                                                  {
                                                                      Price = product.Product_Price.ToString(),
                                                                      Quantity = product.Quantity.ToString(),
                                                                      Name = product.Product_Name
                                                                  });
                                if (PerOrders.Count() != 0)
                                {
                                    Order.Add(new Orders
                                    {
                                        InvoiceNo = item.InvoiceNo,
                                        InvoiceDate = item.InvoiceDate,
                                        Status = item.Status,
                                        Grand_Total = item.Grand_Total.ToString(),
                                    });
                                    foreach (var PerOrder in PerOrders.ToList())
                                    {
                                        Order.LastOrDefault().Products = PerOrders.ToList();
                                    }
                                }
                            }
                            json = jsSerializer.Serialize(new { retCode = 1, Orders = Order });
                        }
                    }
                }
                catch (Exception ex)
                {

                    json = jsSerializer.Serialize(new { retCode = 0, message = ex.Message });
                }
            }
            else
            {
                json = jsSerializer.Serialize(new { retCode = 0, message = "user not logged in" });
            }
            return json;
        }

        public static List<Country> getCountry()
        {
            List<Country> list = new List<Country>();
            using (var db = new royalbor_hairEntities())
            {
                IQueryable<Country> Countrylist = (from c in db.countries
                                                   orderby c.countryname

                                                   select new Country
                                                   {
                                                       CountryName = c.countryname,
                                                       CountryCode = c.code
                                                   });
                list = Countrylist.ToList();
            }
            return list;
        }

        public static List<State> getState(string Code)
        {
            List<State> list = new List<State>();
            using (var db = new royalbor_hairEntities())
            {
                IQueryable<State> Countrylist = (from s in db.states
                                                 where s.countryID == Code

                                                 select new State
                                                 {
                                                     StateName = s.stateName,
                                                     StateId = s.stateID
                                                 });
                list = Countrylist.ToList();
            }
            return list;
        }
        public static List<City> getCity(int Id)
        {
            List<City> list = new List<City>();
            using (var db = new royalbor_hairEntities())
            {
                IQueryable<City> Countrylist = (from c in db.cities
                                                where c.stateID == Id

                                                select new City
                                                {
                                                    CityName = c.cityName,
                                                });
                list = Countrylist.ToList();
            }
            return list;
        }  

        public static string GetCouponPrice(Decimal OfferValue, Decimal TotalPrice)
        {
            string jsonString = "";
            AddToCart objAddToCart = new AddToCart();
            objAddToCart = new AddToCart();
            objAddToCart = (AddToCart)HttpContext.Current.Session["AddToCarts"];



            objAddToCart.SubTotal = TotalPrice.ToString();
             objAddToCart.OfferValue = OfferValue;
            HttpContext.Current.Session["AddToCarts"] = objAddToCart;

            jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            return jsonString;



        }
    }

    public partial class Product
    {
        public string OrderId { get; set; }
        public string OrderDate { get; set; }
        public string PickupLocation { get; set; }
        public string ChannelId { get; set; }
        public string Comment { get; set; }
        public string BillingCustomerName { get; set; }
        public string BillingLastName { get; set; }
        public string BillingAddress { get; set; }
        public string BillingAddress2 { get; set; }
        public string BillingCity { get; set; }
        public string BillingPincode { get; set; }
        public string BillingState { get; set; }
        public string BillingCountry { get; set; }
        public string BillingEmail { get; set; }
        public string BillingPhone { get; set; }
        public string ShippingIsBilling { get; set; }
        public string ShippingCustomerName { get; set; }
        public string ShippingLastName { get; set; }
        public string ShippingAddress { get; set; }
        public string ShippingAddress2 { get; set; }
        public string ShippingCity { get; set; }
        public string ShippingPincode { get; set; }
        public string ShippingCountry { get; set; }
        public string ShippingState { get; set; }
        public string ShippingEmail { get; set; }
        public string ShippingPhone { get; set; }
        public OrderItem[] OrderItems { get; set; }
        public string PaymentMethod { get; set; }
        public Int32 ShippingCharges { get; set; }
        public Int32 GiftwrapCharges { get; set; }
        public Int32 TransactionCharges { get; set; }
        public decimal TotalDiscount { get; set; }
        public decimal SubTotal { get; set; }
        public double Length { get; set; }
        public double Breadth { get; set; }
        public double Height { get; set; }
        public double Weight { get; set; }
    }

    public partial class OrderItem
    {
        public string Name { get; set; }
        public string Sku { get; set; }
        public string Units { get; set; }
        public string SellingPrice { get; set; }
        public string Discount { get; set; }
        public string Tax { get; set; }
        public string Hsn { get; set; }
    }


    public class AddToCart
    {
        public List<listItem> ItemList { get; set; }
        public int ItemCount;
        public string SubTotal { get; set; }

        public decimal OfferValue { get; set; }
    }

    public class Orders
    {
        public string InvoiceNo;
        public string InvoiceDate;
        public string Status;
        public string Grand_Total;
        public List<Products> Products { get; set; }
    }

    public class Products
    {
        public string Price;
        public string Quantity;
        public string Name;
    }

    public class listItem
    {
        public string productName;
        public string imageUrl;
        public Int64 quantity;
        public Decimal price;
        public Decimal subTotal;
        public string productCode;
        public string Desc;
    }

    public class Country
    {
        public string CountryName { get; set; }
        public string CountryCode { get; set; }
    }

    public class State
    {
        public string StateName { get; set; }
        public int StateId { get; set; }
    }

    public class City
    {
        public string CityName { get; set; }
    }

}