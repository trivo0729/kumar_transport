﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace ecommerce.classes
{
    public class WebClint
    {
        public static bool GetReponse(string requestData, string Url, string Type, out string responseXML)
        {
            responseXML = ""; string url = "";
            try
            {

                //*** Local ** ///
                url = Url + Type;
                byte[] data = Encoding.UTF8.GetBytes(requestData);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Timeout = 300000;
                request.Method = "POST";
                request.ContentType = "application/json";
                // request.Headers.Add("Accept-Encoding", "gzip");
               // request.Headers.Add("Content-Type", "application/json");
                Stream dataStream = request.GetRequestStream();
                dataStream.Write(data, 0, data.Length);
                dataStream.Close();
                WebResponse webResponse = request.GetResponse();
                var rsp = webResponse.GetResponseStream();
                using (StreamReader readStream = new StreamReader(rsp))
                {
                    responseXML = readStream.ReadToEnd().ToString();
                }
                return true;
            }
            catch (WebException webEx)
            {
                Console.WriteLine(webEx.Response);
                //get the response stream
                WebResponse response = webEx.Response;
                Stream stream = response.GetResponseStream();
                String responseMessage = new StreamReader(stream).ReadToEnd();
                return false;
            }
        }


        public static bool GetReponseApi(string requestData, string Url, string Type, out string responseXML)
        {
            responseXML = ""; string url = "";
            try
            {

                //*** Local ** ///
                url = Url + Type;
                byte[] data = Encoding.UTF8.GetBytes(requestData);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Timeout = 300000;
                request.Method = "POST";
                request.ContentType = "application/json";
                // request.Headers.Add("Accept-Encoding", "gzip");
                 request.Headers.Add("Authorization", "Bearer {{eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjE0MjQ5OCwiaXNzIjoiaHR0cHM6Ly9hcGl2Mi5zaGlwcm9ja2V0LmluL3YxL2V4dGVybmFsL2F1dGgvbG9naW4iLCJpYXQiOjE1NzY5Mjc2MjgsImV4cCI6MTU3Nzc5MTYyOCwibmJmIjoxNTc2OTI3NjI4LCJqdGkiOiJRWHhFWFVtNDJua2c1NzVvIn0.YdFv0k8W1oBCZ_HekIE2-b2E2OJcQmfeHVjnb-Zjixo}}");
                Stream dataStream = request.GetRequestStream();
                dataStream.Write(data, 0, data.Length);
                dataStream.Close();
                WebResponse webResponse = request.GetResponse();
                var rsp = webResponse.GetResponseStream();
                using (StreamReader readStream = new StreamReader(rsp))
                {
                    responseXML = readStream.ReadToEnd().ToString();
                }
                return true;
            }
            catch (WebException webEx)
            {
                Console.WriteLine(webEx.Response);
                //get the response stream
                WebResponse response = webEx.Response;
                Stream stream = response.GetResponseStream();
                String responseMessage = new StreamReader(stream).ReadToEnd();
                return false;
            }
        }

   
 ///  request.AddParameter("application/json", "{\n  \"order_id\": \"224-477\",\n  \"order_date\": \"2019-07-24 11:11\",\n  \"pickup_location\": \"Jammu\",\n  \"channel_id\": \"12345\",\n  \"comment\": \"Reseller: M/s Goku\",\n  \"billing_customer_name\": \"Naruto\",\n  \"billing_last_name\": \"Uzumaki\",\n  \"billing_address\": \"House 221B, Leaf Village\",\n  \"billing_address_2\": \"Near Hokage House\",\n  \"billing_city\": \"New Delhi\",\n  \"billing_pincode\": \"110002\",\n  \"billing_state\": \"Delhi\",\n  \"billing_country\": \"India\",\n  \"billing_email\": \"naruto@uzumaki.com\",\n  \"billing_phone\": \"9876543210\",\n  \"shipping_is_billing\": true,\n  \"shipping_customer_name\": \"\",\n  \"shipping_last_name\": \"\",\n  \"shipping_address\": \"\",\n  \"shipping_address_2\": \"\",\n  \"shipping_city\": \"\",\n  \"shipping_pincode\": \"\",\n  \"shipping_country\": \"\",\n  \"shipping_state\": \"\",\n  \"shipping_email\": \"\",\n  \"shipping_phone\": \"\",\n  \"order_items\": [\n    {\n      \"name\": \"Kunai\",\n      \"sku\": \"chakra123\",\n      \"units\": 10,\n      \"selling_price\": \"900\",\n      \"discount\": \"\",\n      \"tax\": \"\",\n      \"hsn\": 441122\n    }\n  ],\n  \"payment_method\": \"Prepaid\",\n  \"shipping_charges\": 0,\n  \"giftwrap_charges\": 0,\n  \"transaction_charges\": 0,\n  \"total_discount\": 0,\n  \"sub_total\": 9000,\n  \"length\": 10,\n  \"breadth\": 15,\n  \"height\": 20,\n  \"weight\": 2.5\n}",  ParameterType.RequestBody);

//        var client = new RestClient("https://apiv2.shiprocket.in/v1/external/orders/create/adhoc");
//        client.Timeout = -1;
//var request = new RestRequest(Method.POST);
//        request.AddHeader("Content-Type", "application/json");
//request.AddHeader("Authorization", "Bearer {{token}}");
//request.AddParameter("application/json", "{\n  \"order_id\": \"224-447\",\n  \"order_date\": \"2019-07-24 11:11\",\n  \"pickup_location\": \"Jammu\",\n  \"channel_id\": \"\",\n  \"comment\": \"Reseller: M/s Goku\",\n  \"billing_customer_name\": \"Naruto\",\n  \"billing_last_name\": \"Uzumaki\",\n  \"billing_address\": \"House 221B, Leaf Village\",\n  \"billing_address_2\": \"Near Hokage House\",\n  \"billing_city\": \"New Delhi\",\n  \"billing_pincode\": \"110002\",\n  \"billing_state\": \"Delhi\",\n  \"billing_country\": \"India\",\n  \"billing_email\": \"naruto@uzumaki.com\",\n  \"billing_phone\": \"9876543210\",\n  \"shipping_is_billing\": true,\n  \"shipping_customer_name\": \"\",\n  \"shipping_last_name\": \"\",\n  \"shipping_address\": \"\",\n  \"shipping_address_2\": \"\",\n  \"shipping_city\": \"\",\n  \"shipping_pincode\": \"\",\n  \"shipping_country\": \"\",\n  \"shipping_state\": \"\",\n  \"shipping_email\": \"\",\n  \"shipping_phone\": \"\",\n  \"order_items\": [\n    {\n      \"name\": \"Kunai\",\n      \"sku\": \"chakra123\",\n      \"units\": 10,\n      \"selling_price\": \"900\",\n      \"discount\": \"\",\n      \"tax\": \"\",\n      \"hsn\": 441122\n    }\n  ],\n  \"payment_method\": \"Prepaid\",\n  \"shipping_charges\": 0,\n  \"giftwrap_charges\": 0,\n  \"transaction_charges\": 0,\n  \"total_discount\": 0,\n  \"sub_total\": 9000,\n  \"length\": 10,\n  \"breadth\": 15,\n  \"height\": 20,\n  \"weight\": 2.5\n}",  ParameterType.RequestBody);
//IRestResponse response = client.Execute(request);
//        Console.WriteLine(response.Content);

    }
}