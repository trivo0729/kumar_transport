﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Services.aspx.cs" Inherits="Services" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <!--Wrapper Content Start-->
        <div id="wrapper">
           

            <!--Banner Start-->
            <div class="cp_inner-banner">
                <img src="images/banner/inner-banner-img-09.jpg" alt="">
                <!--Inner Banner Holder Start-->
                <div class="cp-inner-banner-holder">
                    <div class="container">
                    <h2>Services</h2>
                      <!--Breadcrumb Start-->
                       <ul class="breadcrumb">
                         <li><a href="Default.aspx">Home</a></li>
                         <li class="active">Services</li>
                       </ul><!--Breadcrumb End-->
                    </div>
              
                </div><!--Inner Banner Holder End-->
                <%--<div class="animate-bus">
                    <img src="images/animate-bus2.png" alt="">
                </div>--%>
            </div>
           <!--Banner End-->

            <!--Main Content Start-->
            <div id="cp-main-content">

                <!--Blog Section Start-->
                <section class="cp-blog-section pd-tb80">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-11">
                                <!--Blog Item Start-->
                                <article class="cp-blog-item">
                                    <figure class="cp-thumb">
                                        <img src="images/service2.jpg" alt="">
                                    </figure>
                                    <div class="cp-text">
                                        <div class="cp-top">
                                            
                                            <h3>Fleet Owners & Transport Contractor
</h3>
                                        </div>
                                        <p>In the transportation service sector, we are one of the fastest growing fleet owners and transportation contractor. We provide timely and flexible transport services and contracts with the help of our dedicated and sincere team. We have own fleet of Trucks / Trailors /Tempos amd more that 35 Vehicles attached with out group .
</p>
                                    
                                        <a href="#" class="cp-btn-style2">Read More</a>
                                    </div>
                                </article><!--Blog Item End-->

                                <!--Blog Item Start-->
                                <article class="cp-blog-item">
                                    <figure class="cp-thumb">
                                        <img src="images/service5.jpg" alt="">
                                    </figure>
                                    <div class="cp-text">
                                        <div class="cp-top">
                                            
                                            <h3>Commission Agents</h3>
                                        </div>
                                        <p>We have our designated person who held responsible for handling shipments and cargo and outsource truck on Commission basis as per requirement in market 
 </p>
                                       
                                        <a href="#" class="cp-btn-style2">Read More</a>
                                    </div>
                                </article><!--Blog Item End-->

                              
                                <article class="cp-blog-item">
                                    <figure class="cp-thumb">
                                        <img src="images/service1.jpg" alt="">
                                    </figure>
                                    <div class="cp-text">
                                        <div class="cp-top">
                                            
                                            <h3>Goods Transportation Service
</h3>
                                        </div>
                                        <p>Our professionals provide swift Goods Transportation Service as per the requirements of the clients. we can easily transport various goods and raw material like construction material, chemicals, textile, steel, hardware, daily needs etc.

 </p>
                                       
                                        <a href="#" class="cp-btn-style2">Read More</a>
                                    </div>
                                </article>

                                <article class="cp-blog-item">
                                    <figure class="cp-thumb">
                                        <img src="images/service3.jpg" alt="">
                                    </figure>
                                    <div class="cp-text">
                                        <div class="cp-top">
                                            
                                            <h3>Bulk Goods Transportation Service
</h3>
                                        </div>
                                        <p>With the support of an experienced and responsible team of professionals, we have been successfully able to handle bulk cargo. This is delivered in a hassle free and safe manner within the mentioned time period at the clients’ end. Our each client is contented with our range and gets back to us for the other projects.

 </p>
                                       
                                        <a href="#" class="cp-btn-style2">Read More</a>
                                    </div>
                                </article>
                              
                             
                                   
                                </div><!--Pagination End-->
                            
                             <div class="col-md-3">
                                <!--Asidebar Outer Start-->
                                
                                <!--Asidebar Outer End-->
                            </div>
                        </div>
                    </div>
                </section><!--Blog Section End-->

            </div><!--Main Content End-->

         

        </div><!--Wrapper Content End-->
</asp:Content>

