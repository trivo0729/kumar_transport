﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ContactUs.aspx.cs" Inherits="ContactUs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
       <!--Wrapper Content Start-->
    <script>
        function User_SendMail() {
            debugger;
            var validate = true;
            if ($("#name").val() == "") {
                $("#name").focus();
                alert("Please enter your Name");
                validate = false;
                return false;
            }

            else if ($("#email").val() == "") {
                $("#email").focus();
                alert("Please enter your Email");
                validate = false;
                return false;
            }
            else if ($("#email").val() != "") {
                var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                if (!emailReg.test($("#email").val())) {
                    $("#email").focus();
                    alert("Please enter valid Email ID");
                    return false;
                }
                else if ($("#text").val() == "") {
                    $("#text").focus();
                    alert("Please enter your Message");
                    validate = false;
                    return false;
                }
            }
            var test = $("#name").val();
            var test1 = $("#email").val();
            var test2 = $("#web").val();
            var test3 = $("#sub").val();
            var test4 = $("#text").val();
            if (validate) {
                var Data = {
                    sName: $("#name").val(),
                    sEmail: $("#email").val(),
                    web: $("#web").val(),
                    sSub: $("#sub").val(),
                    sMessage: $("#text").val(),
                }
                var Jason = JSON.stringify(Data);
                $.ajax({
                    type: "POST",
                    url: "WebService.asmx/EmailSending",
                    //beforeSend: function () {
                    //    $('#wait').css('display', 'inline');
                    //},
                    //complete: function () {
                    //    $('#wait').css('display', 'none');
                    //},
                    data: Jason,
                    //data: '{"sName":"' + $("#name").val() + '","sEmail":"' + $("#email").val() + '","sPhone":"' + $("#number").val() + '","sSubject":"' + $("#sub").val() + '","sMessage":"' + $("#text").val() + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        try {
                            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                            if (result.retCode == 1) {
                                $("#name").val(' ');
                                $("#email").val(' ');
                                $("#web").val(' ');
                                $("#sub").val(' ');
                                $("#text").val('');
                                alert("Your request sent.We will get in touch with you soon.");
                            }
                            else {
                                alert("Your request sent.We will get in touch with you soon");
                            }

                        } catch (e) { }
                    },
                    error: function () {
                        alert("Your request sent.We will get in touch with you soon");
                    }
                });
                return validate;
            }

        }
    </script>
        <div id="wrapper">
           

            <div class="cp_inner-banner" id="cp-inner-contact">
               <!--Inner Map Start-->
               <%--<div id="cp-inner-map"></div>--%>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3720.950534677692!2d79.11317231452026!3d21.154366738855202!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bd4c733faa37271%3A0x865a7a8c47946261!2sKumar+Transport+Corporation!5e0!3m2!1sen!2sin!4v1467186950566" width="1500" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
               <!--Inner Map End-->
            </div>

            <!--Main Content Start-->
            <div id="cp-main-content">

                <!--Contact Us Section Start-->
                <section class="cp-contact-us-section cp-contact-us-section2 pd-b80">
                    <div class="container">
                        <!--Contact Inner Holder Start-->
                        <div class="cp-contact-inner-holder">
                           <!--Get In Outer Start-->
                           <div class="cp-get-in-outer">
                                <div class="row">
                                    <div class="col-md-4 col-sm-4">
                                        <div class="inner-holder">
                                            <i class="fa fa-paper-plane"></i>
                                            <a href="mailto:info@kumartransport.in">info@kumartransport.in</a>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        <div class="inner-holder">
                                            <i class="fa fa-map-marker"></i>
                                            <p>
Masurkar Road, Opp. Balaji Mandir, Itwari, Nagpur 440008, Maharashtra.</p>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        <div class="inner-holder">
                                            <i class="fa fa-phone"></i>
                                            <p>0712-2771770,2730384,27771778</p>
                                            <i class="fa fa-phone"></i>
                                            <p>9921228769,93662433</p>
                                        </div>
                                    </div>
                                </div>
                            </div><!--Get In Outer End-->

                            <div class="cp-heading-style1">
                                <h2>Send Us <span>Feedback</span></h2>
                            </div>

                            <!--Form Box Start-->
                            <div class="cp-form-box cp-form-box2">
                                <h3>Leave a Reply</h3>
                                <form action="form.php" method="post">
                                    <div class="row">
                                        <div class="col-md-3 col-sm-6">
                                            <div class="inner-holder">
                                                <input type="text" placeholder="Your Name" id="name" name="name" required pattern="[a-zA-Z ]+">
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6">
                                            <div class="inner-holder">
                                                <input type="text" placeholder="Email Address" id="email" name="email" required pattern="^[a-zA-Z0-9-\_.]+@[a-zA-Z0-9-\_.]+\.[a-zA-Z0-9.]{2,5}$">
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6">
                                            <div class="inner-holder">
                                                <input type="text" placeholder="Subject" id="sub" name="subject" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6">
                                            <div class="inner-holder">
                                                <input type="text" placeholder="Website" id="web" name="website" required>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12">
                                            <div class="inner-holder">
                                                <textarea placeholder="Message" id="text" name="comment" required></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12">
                                            <div class="inner-holder">
                                                <button type="button" class="btn-submit" onclick="User_SendMail()" value="Submit">Send</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div><!--Form Box End-->
                        </div><!--Contact Inner Holder End-->
                    </div>
                </section><!--Contact Us Section End-->

            </div><!--Main Content End-->

             

        </div><!--Wrapper Content End-->
</asp:Content>

