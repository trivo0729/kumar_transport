﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="wrapper">
        <!--Banner Start-->
        <div class="cp_banner">
            <!--Banner Slider Start-->
            <div class="owl-carousel" id="cp_banner-slider">
                <!--Banner Item Holder Start-->
                <div class="item">
                    <img src="images/D.jpg" alt="">
                    <!--Banner Caption Start-->
                    <%--<div class="cp-banner-caption">
                            <div class="container">
                                <div class="banner-inner-holder">
                                    <strong class="banner-title">Book a Car</strong>
                                    <h2>A rELIABLE WAY TO TRAVEL</h2>
                                    <a href="#" class="cp-btn-style1">Search Now</a>
                                </div>banner-before-img
                            </div>
                        </div>--%><!--Banner Caption End-->
                </div>
                <!--Banner Item Holder End-->

                <!--Banner Item Holder Start-->
                <div class="item">
                    <img src="images/slider002.jpg" alt="">
                    <!--Banner Caption Start-->
                    <%--<div class="cp-banner-caption">
                            <div class="container">
                                <div class="banner-inner-holder">
                                    <strong class="banner-title">Save Time</strong>
                                    <h2>Save time when you arrive!</h2>
                                    <a href="#" class="cp-btn-style1">Search Now</a>
                                </div>
                            </div>
                        </div>--%><!--Banner Caption End-->
                </div>
                <!--Banner Item Holder End-->

                <!--Banner Item Holder Start-->
                <div class="item">
                    <img src="images/slidero2.jpg" alt="">
                    <!--Banner Caption Start-->
                    <%--<div class="cp-banner-caption">
                            <div class="container">
                                <div class="banner-inner-holder">
                                    <strong class="banner-title">Get Startd!</strong>
                                    <h2>Rent for a month. Save $50.</h2>
                                    <a href="#" class="cp-btn-style1">Search Now</a>
                                </div>
                            </div>
                        </div>--%><!--Banner Caption End-->
                </div>
                <!--Banner Item Holder End-->

            </div>
            <!--Banner Slider End-->
        </div>
        <!--Banner End-->
        <!--Main Content Start-->
        <div id="cp-main-content">



            <!--Why Choose Section Start-->
            <section class="cp-why-choose-section2 pd-tb80">
                <div class="container">
                    <div class="row">
                        <div class="col-md-7 col-sm-12">
                            <figure class="cp-thumb choose-thumb">
                                <img src="images/kt01.png" alt="" class="img-circle">
                            </figure>
                        </div>
                        <div class="col-md-5 col-sm-12">
                            <!--Why Choose Text-->
                            <div class="cp-why-choose-text">

                                <h2><span>Kumar Transport </span>Corporation </h2>
                                <p>
                                    We are a Fleet Owner, Corporate Transport Contractor to all Over vidarbha region(Specially in <a href="http://www.kumartransport.in/DeliveryOffices.aspx">Chandarpur District</a> and  <a href="http://www.kumartransport.in/DeliveryOffices.aspx">Gadchiroli District</a>) & Local, We Offer Full Truck load Services,Part Truck Load Service and Semi Part Load Truck Service (Locally known as "<i>"Chiller"</i> booking). We represent quality to our customers. We provide our customers transportation services that meet all mutually established requirements in a safe manner, every time.<br />
                                    We provide a hassle-free environment for customers. In business for over 45 years, is one of the most experienced carriers in the Vidarbha region. Our experienced and professional leadership teams have the knowledge and capabilities to provide the highest level of transportation services to our customers.
                                </p>

                            </div>
                            <!--Why Choose Text-->
                        </div>
                    </div>
                </div>
            </section>
            <!--Why Choose Section End-->

            <!--News & Faq Section Start-->
            <section class="cp-faq-section pd-tb80">
                <div class="container">
                    <div class="cp-heading-style1">
                        <h2>Why Choose <span>Us?</span></h2>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <!--Testimonial Outer Start-->
                            <div class="cp-testimonial-outer">
                                <div class="owl-carousel" id="cp-test-slider2">
                                    <div class="item">
                                        <!--Testimonial Box Strat-->
                                        <div class="cp-testimonial-box">

                                            <h4>In business for over 45 years,</h4>
                                            <p>In business for over 45 years . The Company was started from a small concept and has now transformed into a leading transport company By Grace of God and Great Effort,HardWork and Dedication of Late Syed Aqeeluddin.</p>
                                            <div class="test-bottom">
                                                <!--<div class="thumb">
                                                    <img class="img-circle" src="images/test-sm-thumb.jpg" alt="">
                                                </div>-->

                                            </div>
                                        </div>
                                        <!--Testimonial Box End-->
                                    </div>
                                    <div class="item">
                                        <!--Testimonial Box Strat-->
                                        <div class="cp-testimonial-box">

                                            <h4>leading transporters in Vidarbha region</h4>
                                            <p>Having our Head Office in Nagpur, <a href="http://www.kumartransport.in/DeliveryOffices.aspx">Branch Offices</a> and Associated Offices in some localities. We have trained staff and skilled labor to handle your consignments and cargo in the safest manner.</p>
                                            <div class="test-bottom">
                                                <!--<div class="thumb">
                                                    <img class="img-circle" src="images/test-sm-thumb.jpg" alt="">
                                                </div>-->

                                            </div>
                                        </div>
                                        <!--Testimonial Box End-->
                                    </div>

                                </div>

                            </div>
                            <!--Testimonial Outer End-->
                        </div>
                        <div class="col-md-6">
                            <!--Accordian Item Start-->
                            <div class="cp-accordian-item">
                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="cp-tab-One">
                                            <div class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#cp-tab-collapse1" aria-expanded="true" aria-controls="cp-tab-collapse1">Delivery and Safety?
                                                </a>
                                            </div>
                                        </div>
                                        <div id="cp-tab-collapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="cp-tab-One">
                                            <div class="panel-body">
                                                <div class="cp-thumb">
                                                    <img src="images/car-sm-01.jpg" alt="">
                                                </div>
                                                <div class="cp-text">
                                                    <p>
                                                        <ul>
                                                            <li>Timeliness in delivery.</li>
                                                            <li>Safety First Policy.</li>
                                                        </ul>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="cp-tab-Two">
                                            <div class="panel-title">
                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#cp-tab-collapse2" aria-expanded="false" aria-controls="cp-tab-collapse2">Bussiness Experiance ?
                                                </a>
                                            </div>
                                        </div>
                                        <div id="cp-tab-collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="cp-tab-Two">
                                            <div class="panel-body">
                                                <div class="cp-thumb">
                                                    <img src="images/car-sm-01.jpg" alt="">
                                                </div>
                                                <div class="cp-text">
                                                    <p>
                                                        <ul>
                                                            <li>Huge experience in this sector.
                                                            </li>
                                                            <li>Strong business associations.
                                                            </li>
                                                        </ul>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="cp-tab-Three">
                                            <div class="panel-title">
                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#cp-tab-collapse3" aria-expanded="false" aria-controls="cp-tab-collapse3">Team?
                                                </a>
                                            </div>
                                        </div>
                                        <div id="cp-tab-collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="cp-tab-Three">
                                            <div class="panel-body">
                                                <div class="cp-thumb">
                                                    <img src="images/car-sm-01.jpg" alt="">
                                                </div>
                                                <div class="cp-text">
                                                    <p>
                                                        Experienced team of professionals.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="cp-tab-Four">
                                            <div class="panel-title">
                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#cp-tab-collapse4" aria-expanded="false" aria-controls="cp-tab-collapse4">Quality?
                                                </a>
                                            </div>
                                        </div>
                                        <div id="cp-tab-collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="cp-tab-Four">
                                            <div class="panel-body">
                                                <div class="cp-thumb">
                                                    <img src="images/car-sm-01.jpg" alt="">
                                                </div>
                                                <div class="cp-text">
                                                    <p>
                                                        <ul>
                                                            <li>Quality Assurance.</li>
                                                            <li>Clients Satisfaction.</li>
                                                        </ul>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!--Accordian Item End-->
                        </div>
                    </div>
                </div>
            </section>
            <!--News & Faq Section End-->
            <!--Services Section Start-->
            <section class="cp-services-section pd-tb80">
                <div class="container">
                    <div class="cp-heading-style1">
                        <h2>Our Best <span>Services</span></h2>
                    </div>
                    <!--Why Choose Listed-->
                    <ul class="cp-why-choose-listed">
                        <li>
                            <div class="cp-box">
                                <img src="images/why-choose-img-01.png" alt="">
                                <h3>Fleet Owners & Transport Contractor</h3>
                                <span class="icon-cash19 icomoon"></span>
                                <p>In the transportation service sector, we are one of the fastest growing fleet owners and transportation contractor. We provide timely and flexible transport services and contracts with the help of our dedicated and sincere team. We have own fleet of Trucks / Trailors /Tempos amd more that 35 Vehicles attached with out group .</p>
                                <a href="Services.aspx" class="readmore">Read More</a>
                            </div>
                        </li>
                        <li>
                            <div class="cp-box cp-box2">
                                <img src="images/why-choose-img-03.png" alt="">
                                <h3>Bulk Goods Transportation Service
                                </h3>
                                <span class="icon-transport1105 icomoon"></span>
                                <p>
                                    With the support of an experienced and responsible team of professionals, we have been successfully able to handle bulk cargo. This is delivered in a hassle free and safe manner within the mentioned time period at the clients’ end. Our each client is contented with our range and gets back to us for the other projects.<br />

                                </p>
                                <a href="Services.aspx" class="readmore">Read More</a>
                            </div>
                        </li>

                    </ul>
                    <!--Why Choose Listed End-->
                </div>
                <div class="container">
                    <div class="cp-heading-style1">
                        <!--<h2>Our Best <span>Services</span></h2>-->
                    </div>
                    <!--Why Choose Listed-->
                    <ul class="cp-why-choose-listed">
                        <li>
                            <div class="cp-box">
                                <img src="images/why-choose-img-01.png" alt="">
                                <h3>Goods Transportation Service
                                </h3>
                                <span class="icon-house158 icomoon"></span>
                                <p>Our professionals provide swift Goods Transportation Service as per the requirements of the clients. we can easily transport various goods and raw material like construction material, chemicals, textile, steel, hardware, daily needs etc.</p>
                                <a href="Services.aspx" class="readmore">Read More</a>
                            </div>
                        </li>
                        <li>
                            <div class="cp-box cp-box2">
                                <img src="images/why-choose-img-03.png" alt="">
                                <h3>Commission Agents
                                </h3>
                                <span class="icon-transport1105 icomoon"></span>
                                <p>We have our designated person who held responsible for handling shipments and cargo and outsource truck on Commission basis as per requirement in market.</p>
                                <br />

                                <a href="Services.aspx" class="readmore">Read More</a>
                            </div>
                        </li>

                    </ul>
                    <!--Why Choose Listed End-->
                </div>
            </section>
            <!--Services Section End-->






            <!--Wrapper Content End-->
            <!--Testimonial Section Start-->
            <section class="cp-testimonial-section pd-tb80">
                <div class="container">
                    <div class="cp-heading-style2">
                        <h2>Customer <span>Reviews</span></h2>
                    </div>
                    <div class="owl-carousel" id="cp-testimonial-slider">
                        <div class="item">
                            <div class="cp-testimonial-inner">
                                <div class="row">
                                    <div class="col-md-8 col-sm-8">
                                        <div class="cp-text">
                                            <strong><a href="http://www.justdial.com/nagpur/Kumar-Transport-Cropration-%3Cnear%3E-Itwari/0712PX712-X712-120629124230-E3U5_BZDET">Click Here to Check Star Rating in Just Dial </a></strong>
                                            <%--<span class="cp-icon">--%>
                                                <img src="images/stars.png" style="width:50%" />
                                                <br />
                                            <%--</span>--%>

                                            <%-- <blockquote class="cp-blockquote">
                                                Excellent 
                                            </blockquote>--%>
                                            <span>Mr Vilas Vidhate</span>
                                        </div>
                                    </div>
                                    <%--<div class="col-md-4 col-sm-4">
                                        <span class="cp-icon">
                                            <i class="fa fa-star"></i>
                                        </span>
                                    </div>--%>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="cp-testimonial-inner">
                                <div class="row">
                                    <div class="col-md-8 col-sm-8">
                                        <div class="cp-text">
                                            <strong><a href="http://www.justdial.com/nagpur/Kumar-Transport-Cropration-%3Cnear%3E-Itwari/0712PX712-X712-120629124230-E3U5_BZDET">Click Here to Check Star Rating in Just Dial </a></strong>
                                            <img src="images/stars.png" style="width:50%" />
                                                <br />
                                            <%--<blockquote class="cp-blockquote">
                                                Excellent service n very fair of returning claims of goods damage.
                                            </blockquote>--%>
                                            <span>Shahbaz Akbani</span>
                                        </div>
                                    </div>
                                    <%-- <div class="col-md-4 col-sm-4">
                                        <span class="cp-icon">
                                            <i class="fa fa-star"></i>
                                        </span>
                                    </div>--%>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="cp-testimonial-inner">
                                <div class="row">
                                    <div class="col-md-8 col-sm-8">
                                        <div class="cp-text">
                                            <strong><a href="http://www.justdial.com/nagpur/Kumar-Transport-Cropration-%3Cnear%3E-Itwari/0712PX712-X712-120629124230-E3U5_BZDET">Click Here to Check Star Rating in Just Dial </a></strong>
                                            <img src="images/stars.png" style="width:50%" />
                                                <br />

                                            <%-- <blockquote class="cp-blockquote">
                                                Excellent 
                                            </blockquote>--%>
                                            <span>Mr Karan</span>
                                        </div>
                                    </div>
                                    <%-- <div class="col-md-4 col-sm-4">
                                        <span class="cp-icon">
                                            <i class="fa fa-star"></i>
                                        </span>
                                    </div>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--Testimonial Section End-->
        </div>
        <!--Main Content End-->
    </div>
</asp:Content>

