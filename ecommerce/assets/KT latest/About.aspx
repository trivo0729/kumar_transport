﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="About.aspx.cs" Inherits="About" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!--Wrapper Content Start-->
    <div id="wrapper">


        <!--Banner Start-->
        <div class="cp_inner-banner">
            <img src="images/banner/inner-banner-img-04.jpg" alt="">
            <!--Inner Banner Holder Start-->
            <div class="cp-inner-banner-holder">
                <div class="container">
                    <h2>About us</h2>
                    <!--Breadcrumb Start-->
                    <ul class="breadcrumb">
                        <li><a href="Default.aspx">Home</a></li>
                        <li class="active">About us</li>
                    </ul>
                    <!--Breadcrumb End-->
                </div>

            </div>
            <!--Inner Banner Holder End-->
            <%--<div class="animate-bus">
                    <img src="images/animate-bus2.gif" alt="">
                </div>--%>
        </div>
        <!--Banner End-->

        <!--Main Content Start-->
        <div id="cp-main-content">

            <!--Blog Section Start-->
            <section class="cp-blog-section pd-tb80">
                <div class="container">
                    <div class="row">
                        <div class="col-md-9">
                            <!--Blog Detail Outer Start -->
                            <div class="cp-blog-detail-outer">
                                <!--Blog Item Start-->
                                <article class="cp-blog-item">
                                    <%--<figure class="cp-thumb">
                                            <img src="images/blog-thumb-01.jpg" alt="">
                                        </figure>--%>
                                    <div class="cp-text">
                                        <div class="cp-top">
                                            <%--<div class="date-box">
                                                    19
                                                    <span>March</span>
                                                </div>--%>
                                            <h3>About Kumar Transport Corporation
                                            </h3>
                                        </div>
                                        <p>
                                            We please to introduce ourselves as one of the leading transport 
Having our Head Office in Nagpur, having Branches all over vidarbha Region(Specially in <a href="http://www.kumartransport.in/DeliveryOffices.aspx">Chandarpur District</a> and  <a href="http://www.kumartransport.in/DeliveryOffices.aspx">Gadchiroli District</a>) & Local , Since 1971. 
We have own fleet of Trucks / Trailors /Tempos amd more that 35 
Vehicles attached with out group plying all over India. 
Loading of goods is an important task for any kinds of relocation situation. Proper loading of goods avoids the risks of damages. It requires extra care and experience because loading of goods is risky and it is possible goods would be damages while loading by inexpert hands. So to avoid such situations or risks on loading process we at Packers and Movers 
provide safe & secure loading services..
                                        </p>
                                        <blockquote>
                                            <p>
                                                In business for over 45 years . The Company was started from a small concept and has now transformed into a leading transport company By Grace of God and Great Effort,HardWork and Dedication of Late Syed Aqeeluddin.

                                            </p>
                                        </blockquote>
                                        <p>
                                           Our customers believe our existence as Kumar Transports.
We are a trusted and active organization, responsive to the present 
and future needs of our customers. Ensuring Customer Satisfaction is the
reason for our many decades of success in the transportation business.

                                        </p>

                                 

                                        <%-- <ul class="cp-meta-listed">
                                                <li>Written by <a href="#">admin</a></li>
                                                <li>Category:<a href="blog-detail.html">Blog</a></li>
                                            </ul>--%>
                                    </div>
                                </article>
                                <!--Blog Item End-->







                            </div>
                            <!--Blog Detail Outer End -->

                        </div>
                        <div class="col-md-3">
                            <!--Asidebar Outer Start-->
                            <aside class="cp_sidebar-outer">


                                <!--Widget Holder Start-->
                                <div class="widget widget-popular">
                                    <h4>Most Popular</h4>
                                    <ul>
                                        <li>
                                            <div class="cp-popular-holder">
                                                <div class="cp-thumb">
                                                    <img src="images/about1.jpg" alt=""></div>
                                                <%--<div class="cp-text">
                                                        <h5>Shoot inspiration </h5>
                                                        <ul class="listed">
                                                            <li>Monday, Feb 30, 2016</li>
                                                            <li>by Jiana Smith, 110 Views</li>
                                                        </ul>
                                                    </div>--%>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="cp-popular-holder">
                                                <div class="cp-thumb">
                                                    <img src="images/about2.jpg" alt=""></div>
                                                <%--<div class="cp-text">
                                                        <h5>She the wanderner </h5>
                                                        <ul class="listed">
                                                            <li>Monday, Mar 20, 2016</li>
                                                            <li>by Jiana Smith, 160 Views</li>
                                                        </ul>
                                                    </div>--%>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="cp-popular-holder">
                                                <div class="cp-thumb">
                                                    <img src="images/about3.jpg" alt=""></div>
                                                <%--<div class="cp-text">
                                                        <h5>Wheeler is my hero </h5>
                                                        <ul class="listed">
                                                            <li>Monday, Feb 26, 2016</li>
                                                            <li>by Jiana Smith, 210 Views</li>
                                                        </ul>
                                                    </div>--%>
                                            </div>
                                        </li>


                                    </ul>
                                </div>
                                <!--Widget Holder End-->





                            </aside>
                            <!--Asidebar Outer End-->
                        </div>
                    </div>
                </div>
            </section>
            <!--Blog Section End-->

        </div>
        <!--Main Content End-->



    </div>
               <!--Advertising Section Start-->
                 <section class="cp-advertising-section pd-tb80">
                     <div class="container">
                        <%--<div class="cp-heading-style2">
                            <h2>Taxi<span>Advertising</span></h2>
                        </div>--%>
                         <div class="row">
                             <div class="col-md-4 col-sm-12">
                                 <!--Advertising Box Start-->
                                <%-- <div class="cp-advertising-box">
                                     <figure>
                                         <img src="images/advertising-sm-thumb-01.png" alt="" width="100%" height="100%" >
                                        
                                     </figure>
                                     
                                 </div><!--Advertising Box End-->--%>
                             </div>
                              <div class="col-md-4 col-sm-12">
                                 <!--Advertising Box Start-->
                                 <div class="cp-advertising-box">
                                     <figure>
                                         <img src="images/advertising-sm-thumb-01.png" alt="" width="100%">
                                        
                                     </figure>
                                     
                                 </div><!--Advertising Box End-->
                             </div>
                             <div class="col-md-12 cp-btn-holder">
                                 <a href="http://www.justdial.com/nagpur/Kumar-Transport-Cropration-%3Cnear%3E-Itwari/0712PX712-X712-120629124230-E3U5_BZDET" target="_blank" class="cp-btn-style1">More Detail's</a>
                             </div>
                         </div>
                     </div>
                 </section><!--Advertising Section End-->
</asp:Content>

