﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for WebService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class WebService : System.Web.Services.WebService {

    public WebService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(true)]
    public string EmailSending(string sName, string sEmail, string web, string sSub, string sMessage)
    {
        string sJsonString = "{\"retCode\":\"0\"}";
        if (Email_Manager.SendEmail("info@kumartransport.in", sEmail, "Enquiry", "Information Received", sName, sEmail, web, sSub, sMessage) == true)//info@tmauto.in
        {
            sJsonString = "{\"retCode\":\"1\",}";
        }//sales@versusgroupindia.com
        return sJsonString;
    }
    
}
