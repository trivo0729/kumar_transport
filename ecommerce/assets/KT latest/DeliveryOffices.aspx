﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DeliveryOffices.aspx.cs" Inherits="DeliveryOffices" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <!--Wrapper Content Start-->
        <div id="wrapper">


            <!--Banner Start-->
            <div class="cp_inner-banner">
                <img src="images/banner/inner-banner-img-01.jpg" alt="">
                <!--Inner Banner Holder Start-->
                <div class="cp-inner-banner-holder">
                    <div class="container">
                    <h2>Delivery Offices</h2>
                      <!--Breadcrumb Start-->
                       <ul class="breadcrumb">
                         <li><a href="Default.aspx">Home</a></li>
                         <li class="active">Delivery Offices</li>
                       </ul><!--Breadcrumb End-->
                    </div>
              
                </div><!--Inner Banner Holder End-->
               <%-- <div class="animate-bus">
                    <img src="images/animate-bus2.png" alt="">
                </div>--%>
            </div>
           <!--Banner End-->

            <!--Main Content Start-->
            <div id="cp-main-content">

                <!--Taxi Section Start-->
                <%--<section class="cp-taxi-section pd-tb80">
                    <div class="container">
                        <div class="cp-heading-style1">
                            <h2>Branches &  <span>Delivery Offices</span></h2>
                        </div>
                        <div class="cp-tabs-holder">
                         
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="tab-01">
                                <div class="row">
                                    <div class="col-md-4 col-sm-6">
                                        <!--Taxi Holder Start-->
                                        <article class="cp-taxi-holder">
                                            <figure class="cp-thumb">
                                                <img src="images/taxi-img-01.jpg" alt="">
                                            </figure>
                                            <div class="cp-text">
                                                <h3>Chandrapur</h3>
                                                <ul class="cp-meta-listed">
                                                    <li>KT, Chaorala road, Near bimba gate,
rehmat nagar, cement road .
 <span>chandrapur</span></li>
                                                    <li>Contact Person :- <span>Saiyad Imran</span></li>

                                                    <li><p>Phone: <strong>9890347414,07172- &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;202122,9673577988</strong></p></li>
                                                </ul>
<%--                                                <a href="booking.html" class="cp-btn-style1">Book Now</a> --%>
                                            </div>
                                        </article><!--Taxi Holder End-->
                                    </div>
                                     <%--<div class="col-md-4 col-sm-6">
                                        <!--Taxi Holder Start-->
                                        <article class="cp-taxi-holder">
                                            <figure class="cp-thumb">
                                                <img src="images/taxi-img-02.jpg" alt="">
                                            </figure>
                                            <div class="cp-text">
                                                <h3>Toyotie </h3>
                                                <ul class="cp-meta-listed">
                                                    <li>Mileage: <span>140 000 km </span></li>
                                                    <li>Volume capacity: <span>2.3l</span></li>
                                                    <li>Price: <strong>$29.895</strong></li>
                                                </ul>
                                                <a href="booking.html" class="cp-btn-style1">Book Now</a> 
                                            </div>
                                        </article><!--Taxi Holder End-->
                                    </div>--%>
                                     <%--<div class="col-md-4 col-sm-6">
                                        <!--Taxi Holder Start-->
                                        <article class="cp-taxi-holder">
                                            <figure class="cp-thumb">
                                                <img src="images/taxi-img-03.jpg" alt="">
                                            </figure>
                                            <div class="cp-text">
                                                <h3>Fiat 800</h3>
                                                <ul class="cp-meta-listed">
                                                    <li>Mileage: <span>155 000 km</span></li>
                                                    <li>Volume capacity: <span>1.6l</span></li>
                                                    <li>Price: <strong>$35.899</strong></li>
                                                </ul>
                                                <a href="booking.html" class="cp-btn-style1">Book Now</a> 
                                            </div>
                                        </article><!--Taxi Holder End-->
                                    </div>--%>
                                </div>
                            </div>
<%--                            <div role="tabpanel" class="tab-pane fade" id="tab-02">
                               <div class="row">
                                    <div class="col-md-4 col-sm-6">
                                        <!--Taxi Holder Start-->
                                        <article class="cp-taxi-holder">
                                            <figure class="cp-thumb">
                                                <img src="images/taxi-img-02.jpg" alt="">
                                            </figure>
                                            <div class="cp-text">
                                                <h3>Cci-Fiat 500</h3>
                                                <ul class="cp-meta-listed">
                                                    <li>Mileage: <span>130 000 km</span></li>
                                                    <li>Volume capacity: <span>1.3l</span></li>
                                                    <li>Price: <strong>$25.899</strong></li>
                                                </ul>
                                                <a href="booking.html" class="cp-btn-style1">Book Now</a> 
                                            </div>
                                        </article><!--Taxi Holder End-->
                                    </div>
                                     <div class="col-md-4 col-sm-6">
                                        <!--Taxi Holder Start-->
                                        <article class="cp-taxi-holder">
                                            <figure class="cp-thumb">
                                                <img src="images/taxi-img-03.jpg" alt="">
                                            </figure>
                                            <div class="cp-text">
                                                <h3>Toyotie </h3>
                                                <ul class="cp-meta-listed">
                                                    <li>Mileage: <span>140 000 km </span></li>
                                                    <li>Volume capacity: <span>2.3l</span></li>
                                                    <li>Price: <strong>$29.895</strong></li>
                                                </ul>
                                                <a href="booking.html" class="cp-btn-style1">Book Now</a> 
                                            </div>
                                        </article><!--Taxi Holder End-->
                                    </div>
                                     <div class="col-md-4 col-sm-6">
                                        <!--Taxi Holder Start-->
                                        <article class="cp-taxi-holder">
                                            <figure class="cp-thumb">
                                                <img src="images/taxi-img-01.jpg" alt="">
                                            </figure>
                                            <div class="cp-text">
                                                <h3>Fiat 800</h3>
                                                <ul class="cp-meta-listed">
                                                    <li>Mileage: <span>155 000 km</span></li>
                                                    <li>Volume capacity: <span>1.6l</span></li>
                                                    <li>Price: <strong>$35.899</strong></li>
                                                </ul>
                                                <a href="booking.html" class="cp-btn-style1">Book Now</a> 
                                            </div>
                                        </article><!--Taxi Holder End-->
                                    </div>
                                </div>  
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="tab-03">
                                 <div class="row">
                                    <div class="col-md-4 col-sm-6">
                                        <!--Taxi Holder Start-->
                                        <article class="cp-taxi-holder">
                                            <figure class="cp-thumb">
                                                <img src="images/taxi-img-03.jpg" alt="">
                                            </figure>
                                            <div class="cp-text">
                                                <h3>Cci-Fiat 500</h3>
                                                <ul class="cp-meta-listed">
                                                    <li>Mileage: <span>130 000 km</span></li>
                                                    <li>Volume capacity: <span>1.3l</span></li>
                                                    <li>Price: <strong>$25.899</strong></li>
                                                </ul>
                                                <a href="booking.html" class="cp-btn-style1">Book Now</a> 
                                            </div>
                                        </article><!--Taxi Holder End-->
                                    </div>
                                     <div class="col-md-4 col-sm-6">
                                        <!--Taxi Holder Start-->
                                        <article class="cp-taxi-holder">
                                            <figure class="cp-thumb">
                                                <img src="images/taxi-img-01.jpg" alt="">
                                            </figure>
                                            <div class="cp-text">
                                                <h3>Toyotie </h3>
                                                <ul class="cp-meta-listed">
                                                    <li>Mileage: <span>140 000 km </span></li>
                                                    <li>Volume capacity: <span>2.3l</span></li>
                                                    <li>Price: <strong>$29.895</strong></li>
                                                </ul>
                                                <a href="booking.html" class="cp-btn-style1">Book Now</a> 
                                            </div>
                                        </article><!--Taxi Holder End-->
                                    </div>
                                     <div class="col-md-4 col-sm-6">
                                        <!--Taxi Holder Start-->
                                        <article class="cp-taxi-holder">
                                            <figure class="cp-thumb">
                                                <img src="images/taxi-img-02.jpg" alt="">
                                            </figure>
                                            <div class="cp-text">
                                                <h3>Fiat 800</h3>
                                                <ul class="cp-meta-listed">
                                                    <li>Mileage: <span>155 000 km</span></li>
                                                    <li>Volume capacity: <span>1.6l</span></li>
                                                    <li>Price: <strong>$35.899</strong></li>
                                                </ul>
                                                <a href="booking.html" class="cp-btn-style1">Book Now</a> 
                                            </div>
                                        </article><!--Taxi Holder End-->
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="tab-04">
                                 <div class="row">
                                    <div class="col-md-4 col-sm-6">
                                        <!--Taxi Holder Start-->
                                        <article class="cp-taxi-holder">
                                            <figure class="cp-thumb">
                                                <img src="images/taxi-img-03.jpg" alt="">
                                            </figure>
                                            <div class="cp-text">
                                                <h3>Cci-Fiat 700</h3>
                                                <ul class="cp-meta-listed">
                                                    <li>Mileage: <span>120 000 km</span></li>
                                                    <li>Volume capacity: <span>1.3l</span></li>
                                                    <li>Price: <strong>$25.899</strong></li>
                                                </ul>
                                                <a href="booking.html" class="cp-btn-style1">Book Now</a> 
                                            </div>
                                        </article><!--Taxi Holder End-->
                                    </div>
                                     <div class="col-md-4 col-sm-6">
                                        <!--Taxi Holder Start-->
                                        <article class="cp-taxi-holder">
                                            <figure class="cp-thumb">
                                                <img src="images/taxi-img-02.jpg" alt="">
                                            </figure>
                                            <div class="cp-text">
                                                <h3>Toyotie </h3>
                                                <ul class="cp-meta-listed">
                                                    <li>Mileage: <span>140 000 km </span></li>
                                                    <li>Volume capacity: <span>2.3l</span></li>
                                                    <li>Price: <strong>$29.895</strong></li>
                                                </ul>
                                                <a href="booking.html" class="cp-btn-style1">Book Now</a> 
                                            </div>
                                        </article><!--Taxi Holder End-->
                                    </div>
                                     <div class="col-md-4 col-sm-6">
                                        <!--Taxi Holder Start-->
                                        <article class="cp-taxi-holder">
                                            <figure class="cp-thumb">
                                                <img src="images/taxi-img-01.jpg" alt="">
                                            </figure>
                                            <div class="cp-text">
                                                <h3>Fiat 800</h3>
                                                <ul class="cp-meta-listed">
                                                    <li>Mileage: <span>155 000 km</span></li>
                                                    <li>Volume capacity: <span>1.6l</span></li>
                                                    <li>Price: <strong>$35.899</strong></li>
                                                </ul>
                                                <a href="booking.html" class="cp-btn-style1">Book Now</a> 
                                            </div>
                                        </article><!--Taxi Holder End-->
                                    </div>
                                </div>
                            </div>--%>
                          </div>
                        </div>                      
                        
                    </div>
                </section><!--Taxi Section End-->

                <!--Offers Section Start-->
                <section class="cp-offers-section pd-tb80">
                    <div class="container">
                        <div class="cp-heading-style2">
                            <h2>Branches &<span> Delivery Offices </span></h2>
                        </div>
                        <div class="row">
                            <div class="col-md-7 col-md-offset-5">
                                <!--Offers Holder Start-->
                                <div class="cp-offers-holder">
                             <%--       <p>We provide a hassle-free environment for customers. In business for over 40 years, is one of the most experienced carriers in the Vidarbha region. Our experienced and professional leadership teams have the knowledge and capabilities to provide the highest level of transportation services to our customers.

</p>--%>
                                    <ul class="cp-offers-listed">
                                        <li>
                                            <div class="cp-offers-inner">
                                                <span class="icon-holder">
                                                    <i class="icon-house158 icomoon"></i>
                                                </span>
                                                <div class="cp-text">
                                                    <h3>Chandrapur</h3>
                                                    <p>Near Bimba gate,Chorala Cement road,Chandrapur.<br />Contact Person:  <strong>Syed Ikram</strong><br />Phone Number : <strong>9890347414,9823319972, 07172-202122, 9673577988</strong> </p>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="cp-offers-inner">
                                                <span class="icon-holder">
                                                 <i class="icon-house158 icomoon"></i>
                                                </span>
                                                <div class="cp-text">
                                                    <h3>Ghuggus</h3>
                                                    <p>Acc Road,Near Vidya threater Ghuggus.<br />Contact Prson :<strong>Anand Rao Mandre</strong><br />Phone Number : <strong>9623061204</strong>  </p>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="cp-offers-inner">
                                                <span class="icon-holder">
                                                    <i class="icon-house158 icomoon"></i>
                                                </span>
                                                <div class="cp-text">
                                                    <h3>Ballarsha</h3>
                                                    <p>Bhaskarwar PetrolPump,Allapalli Road,Ballarsha<br />Contact Person:  <strong>Mohan Dipake.</strong><br />Phone Number : <strong> 8149927854</strong> </p>
                                                </div>
                                            </div>
                                        </li>
                                           <li>
                                            <div class="cp-offers-inner">
                                                <span class="icon-holder">
                                                    <i class="icon-house158 icomoon"></i>
                                                </span>
                                                <div class="cp-text">
                                                    <h3>Rajura</h3>
                                                      <p>Opp. Bazar Samiti ,Eid-Gha<br />Contact Person:  <strong> Aqueel Bhai</strong><br />Phone Number : <strong> 9921663866</strong> </p>
                                                </div>
                                            </div>
                                        </li>
                                           <li>
                                            <div class="cp-offers-inner">
                                                <span class="icon-holder">
                                                    <i class="icon-house158 icomoon"></i>
                                                </span>
                                                <div class="cp-text">
                                                    <h3>Gadchandur </h3>
                                                     <p>Manikgarh Cement Factory road ,Gadchandur<br />Contact Person:  <strong>Ansar Khan</strong><br />Phone Number : <strong> 9767625313</strong> </p>
                                                </div>
                                            </div>
                                        </li>
                                           <li>
                                            <div class="cp-offers-inner">
                                                <span class="icon-holder">
                                                    <i class="icon-house158 icomoon"></i>
                                                </span>
                                                <div class="cp-text">
                                                    <h3>Korpana </h3>
                                                     <p><br />Contact Person:  <strong>Ansar Khan</strong><br />Phone Number : <strong> 9767625313</strong> </p>
                                                </div>
                                            </div>
                                        </li>
                                          <li>
                                            <div class="cp-offers-inner">
                                                <span class="icon-holder">
                                                    <i class="icon-house158 icomoon"></i>
                                                </span>
                                                <div class="cp-text">
                                                    <h3>Nandafata </h3>
                                                     <p><br />Contact Person:  <strong>Ansar Khan</strong><br />Phone Number : <strong> 9767625313</strong> </p>
                                                </div>
                                            </div>
                                        </li>
                                          <li>
                                            <div class="cp-offers-inner">
                                                <span class="icon-holder">
                                                    <i class="icon-house158 icomoon"></i>
                                                </span>
                                                <div class="cp-text">
                                                    <h3>Aheri </h3>
                                                     <p>ST Aheri Bustand.<br />Contact Person:  <strong>Sadique Bhai</strong><br />Phone Number : <strong> 8605297384</strong> </p>
                                                </div>
                                            </div>
                                        </li>
                                          <li>
                                            <div class="cp-offers-inner">
                                                <span class="icon-holder">
                                                    <i class="icon-house158 icomoon"></i>
                                                </span>
                                                <div class="cp-text">
                                                    <h3>Aalapalli </h3>
                                                     <p>Aalapalli Main Square,Aalapalli<br />Contact Person:  <strong> Sadique Bhai</strong><br />Phone Number : <strong> 8605297384</strong> </p>
                                                </div>
                                            </div>
                                        </li>
                                        
                                          <li>
                                            <div class="cp-offers-inner">
                                                <span class="icon-holder">
                                                    <i class="icon-house158 icomoon"></i>
                                                </span>
                                                <div class="cp-text">
                                                    <h3>Sironcha
 </h3>
                                                     <p><br />Phone Number : <strong> 9421803364,9326662433</strong> </p>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div><!--Offers Holder End-->
                            </div>
                        </div>
                    </div>
                </section><!--Offers Section End-->

               

               

            </div><!--Main Content End-->

         

        </div><!--Wrapper Content End-->
</asp:Content>

