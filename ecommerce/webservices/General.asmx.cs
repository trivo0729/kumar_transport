﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Data;
using classes = ecommerce.classes;
using System.Net;
using System.Text;

namespace ecommerce.webservices
{
    /// <summary>
    /// Summary description for General
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class General : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public string LoginUser(string UserName, string Password)
        {
            string json = "";
            return json = classes.General.LoginUser(UserName, Password);
        }

        [WebMethod(EnableSession = true)]
        public string Registeruser(string FirstName, string LastName, string Email, string Mobile, string Country, string State, string City, string Address, string Zipcode, string Password)
        {
            string json = "";
            return json = classes.General.Registeruser(FirstName, LastName, Email, Mobile, Country, State, City, Address, Zipcode, Password);
        }

        [WebMethod(EnableSession = true)]
        public string GetUserData()
        {
            string json = "";
            return json = classes.General.GetUserData();
        }

        [WebMethod(EnableSession = true)]
        public string ForgetPass(string email)
        {
            string json = "";
            json = classes.General.ForgetPass(email);
            return json;
        }
        [WebMethod(EnableSession = true)]
        public string SendMail(string userName, string nMobile, string email, string subject, string message)
        {
            string json = "";
            json = classes.General.SendMail(userName, nMobile, email, subject, message);
            return json;
        }
        [WebMethod(EnableSession = true)]
        public string AddToCart(string Name, decimal SellingPrice, string Image, string Code, Int64 Qnty)
        {
            string json = "";
            return json = classes.General.AddToCart(Name, SellingPrice, Image, Code, Qnty);
        }

        [WebMethod(EnableSession = true)]
        public string RemoveItem(string productCode)
        {
            string json = "";
            return json = classes.General.RemoveItem(productCode);
        }

        [WebMethod(EnableSession = true)]
        public string getCartItem()
        {
            string json = "";
            return json = classes.General.getCartItem();
        }

        [WebMethod(EnableSession = true)]
        public string getorders()
        {
            string json = "";
            return json = classes.General.getorders();
        }

        [WebMethod(EnableSession = true)]
        public string getCountry()
        {
            string json = "";
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<classes.Country> Country = classes.General.getCountry();
            return json = jsSerializer.Serialize(new { retCode = 1, Country = Country });
        }

        [WebMethod(EnableSession = true)]
        public string getState(string Code)
        {
            string json = "";
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<classes.State> State = classes.General.getState(Code);
            return json = jsSerializer.Serialize(new { retCode = 1, State = State });
        }

        [WebMethod(EnableSession = true)]
        public string getCity(int Id)
        {
            string json = "";
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<classes.City> City = classes.General.getCity(Id);
            return json = jsSerializer.Serialize(new { retCode = 1, City = City });
        }

        [WebMethod(EnableSession = true)]
        public string logout()
        {
            string json = "";
            return json = classes.General.logout();
        }

        [WebMethod(EnableSession = true)]
        public string BookProduct(string FirstName, string LastName, string Email, string Address, string Phone, string City, string PostCode, string Country, string State, string InvoiceNo, string subTotal, string amount, string couponid)
        {
            string json = "";
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            return json = classes.General.BookProduct(FirstName, LastName, Email, Address, Phone, City, PostCode, Country, State, InvoiceNo, subTotal, amount, couponid);
        } 

       
        [WebMethod(EnableSession = true)]
        public string GetCouponPrice(Decimal OfferValue, Decimal TotalPrice)
        {
            string json = "";
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            return json = classes.General.GetCouponPrice(OfferValue, TotalPrice);
        }
     

     
    }
}
