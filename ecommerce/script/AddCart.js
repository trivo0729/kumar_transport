﻿$(document).ready(function () {
    getCartItem();
});


function getCartItem() {
    $.ajax({
        type: "POST",
        url: "webservices/General.asmx/getCartItem",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = JSON.parse(response.d);
            if (result.retCode == 1 && result.listItem.ItemCount != 0) {
                generatecart(result.listItem, result.GrndTotal);
            }
            else
            {
                generateemptycart();
            }
        },
    });
}

function generatecart(items, total) {
    $(".tt-cart-list").empty();
    $(".tt-cart-empty").hide();
    $(".tt-cart-content").show();
    var products = items.ItemList;
    var html = ""
    for (var i = 0; i < products.length; i++) {
        if (i < 3) {
            html += '        <div class="tt-item">'
            html += '            <a href="#">'
            html += '                <div class="tt-item-img">'
            html += '                    <img src="' + products[i].imageUrl + '" data-src="' + products[i].imageUrl + '" alt="">'
            html += '                </div>'
            html += '                <div class="tt-item-descriptions">'
            html += '                    <h2 class="tt-title">' + products[i].productName + '</h2>'
            html += '                    <ul class="tt-add-info">'
            html += '                        <li>Wogo Naturals</li>'
            //html += '                        <li>Vendor: Addidas</li>'
            html += '                    </ul>'
            html += '                    <div class="tt-quantity">' + products[i].quantity + ' X</div> <div class="tt-price">' + products[i].price + '</div>'
            html += '                </div>'
            html += '            </a>'
            html += '            <div class="tt-item-close">'
            html += '                <a style="cursor:pointer;" onclick="RemoveItem(\'' + products[i].productCode + '\');" class="tt-btn-close"></a>'
            html += '            </div>'
            html += '        </div>'
        }
        else {
            html += '<div class="tt-cart-btn">'
            html += '<div class="tt-item">'
            html += '    <a href="cart.html" style="margin-left: 85px;" class="btn-link-02 tt-hidden-mobile">View More</a>'
            html += '    <a href="cart.html" class="btn btn-border tt-hidden-desctope">VIEW More</a>'
            html += '</div>'
            html += '</div>'
            break;
        }
    }
    $(".tt-cart-list").html(html);
    $(".tt-badge-cart").text(items.ItemCount);
    $(".tt-cart-total-price").text(total);
}

function RemoveItem(productCode) {
    var data = { productCode: productCode }
    $.ajax({
        type: "POST",
        url: "webservices/General.asmx/RemoveItem",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1 && result.listItem.ItemCount != 0) {
                generatecart(result.listItem, result.GrndTotal);
                getAmount();
                getCartItems();
            }
            else {
                getAmount();
                getCartItems();
                generateemptycart();
            }
        }
    });
}

function generateemptycart() {
    $(".tt-cart-empty").show();
    $(".tt-cart-content").hide();
    $(".tt-badge-cart").text(0);
}

function AddToCart(Name, SellingPrice, Image, Code, Qnty) {
    var data = { Name: Name, SellingPrice: SellingPrice, Image: Image, Code: Code, Qnty: Qnty }
    $.ajax({
        type: "POST",
        url: "webservices/General.asmx/AddToCart",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1 && result.listItem.ItemCount != 0) {
                generatecart(result.listItem, result.GrndTotal);
            }
            else {
                generateemptycart();
            }
        }
    });
}


function getAmount() {
    $.ajax({
        type: "POST",
        url: "webservices/General.asmx/getCartItem",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = JSON.parse(response.d);
            if (result.retCode == 1 && result.listItem.ItemCount != 0) {
                $("#Amount").text("Rs." + result.GrndTotal);
                $("#SubTotal").text("Rs." + result.GrndTotal);
            }
            else {
                window.location.href = "cart.html";
            }
        },
    });
}

function getCartItems() {
    $.ajax({
        type: "POST",
        url: "webservices/General.asmx/getCartItem",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = JSON.parse(response.d);
            if (result.retCode == 1 && result.listItem.ItemCount != 0) {
                carthtml(result.listItem, result.GrndTotal);
            }
            else {
                emptycart();
            }
        },
    });
}

