﻿var Page_url = "";
$(document).ready(function () {
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
   // var url = atob(url).split('=');
    var url = url[0].split('=')[1].replace(/%20/g, " ").split(',')[0];
    Page_url = url;
});


function LoginUser() {
    var UserName = $("#UserName").val();
    var Password = $("#Password").val();
    var data = { UserName: UserName, Password: Password };
    $.ajax({
        type: "Post",
        url: "webservices/General.asmx/LoginUser",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var response = JSON.parse(response.d);
            if (response.retCode == 1) {
                if (Page_url == "current") {
                    window.location.href = "checkout.html"
                }
                else {
                    window.location.href = Page_url + ".html"
                   
                }
            }
            else {
                alert(response.message);
            }
        }
    });
}

function OnlineRegister() {
    debugger;
    var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    var sUserName = document.getElementById("txtEmail").value;
    var sPassword = document.getElementById("txtPassword").value;
    var sConfirmPassword = document.getElementById("txtCon_Password").value;
    var bValid = true;

    if (sUserName == "") {
        bValid = false;
        alert("User Email cannot be blank");
        document.getElementById('txtEmail').focus();
        return false;
    }
    else if (!re.test(sUserName)) {
        bValid = false;
        alert("The email address you have entered is invalid");
        document.getElementById('txtEmail').focus();
        return false;
    }
    if (sPassword == "") {
        bValid = false;
        alert("Password cannot be blank");
        document.getElementById('txtPassword').focus();
        return false;
    }
    if (sConfirmPassword == "") {
        bValid = false;
        alert("Confirm Password cannot be blank");
        document.getElementById('txtCon_Password').focus();
        return false;
    }
    if (sConfirmPassword != sPassword) {
        bValid = false;
        alert("Password cannot be Matched");
        document.getElementById('txtPassword').focus();
        return false;
    }

    var data = { FirstName: "", LastName: "", Email: sUserName, Mobile: "", Country: "", State: "", City: "", Address: "", Zipcode: "", Password: sPassword }

    if (bValid == true) {
        $.ajax({
            type: "POST",
            url: "webservices/General.asmx/Registeruser",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var response = JSON.parse(response.d);
                if (response.retCode == 1) {
                    alert('You have Successfully registered,Please check your E-mail.')
                    if (Page_url == "current") {
                        window.location.href = "checkout.html"
                    }
                    else {
                        window.location.href = Page_url + ".html"
                    }
                }
                else {
                    alert(response.message);
                }
            },
        });
    }
}

function ForgetPass() {
    var email = $("#txt_frgtEmail").val();
    if (email == "") {
        Success("Please enter Email");
        return false;
    }
    var data = {
        email: email
    }
    $.ajax({
        type: "POST",
        url: "webservices/General.asmx/ForgetPass",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.retCode == 1) {
                alert("Login Details Send successfully,Please check your email");
                window.location = "login.html";
            }
            else if (obj.retCode == 2) {
                alert("This Email is not Registered .");
            }
            else {
                alert("Please try again.");
            }
        }
    });

}

function SendMail() {

    var reg = new RegExp('[0-9]$');
    var regPan = new RegExp('^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$');
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    //var regAddress = new RegExp('^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$');

    var userName = $("#txt_name").val();
    if (userName == "") {
        Success("Please enter User Name");
        return false;
    }


    var email = $("#txt_email").val();
    if (email == "") {
        Success("Please enter Email");
        return false;
    }
    var nMobile = $("#txt_mobile").val();
    if (nMobile == "") {
        bValid = false;
        Success("Please Enter Mobile No");
        return;
    }
    else {
        if (!(reg.test(nMobile))) {
            bValid = false;
            Success("* Mobile no. must be numeric.");
            return;
        }
    }
    var subject = $("#txt_subject").val();
    if (subject == "") {
        Success("Please enter Subject");
        return false;
    }
    var message = $("#txt_message").val();
    if (message == "") {
        Success("Please enter Message");
        return false;
    }
    var data = {
        userName: userName,
        nMobile: nMobile,
        email: email,
        subject: subject,
        message: message
    }
    $.ajax({
        type: "POST",
        url: "webservices/General.asmx/SendMail",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.retCode == 1) {
                alert("Enquiry details send successfully.");
                window.location.reload();
            }
            else {
                alert("Something Went Wrong");
            }
        }
    });


}

