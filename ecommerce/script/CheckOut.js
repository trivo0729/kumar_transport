﻿
var Datt;
var rnd;
var StateList = "";
var bValid = false;

$(document).ready(function () {
    ////**invoice number**////
    RandomNo = getRandomInt(10000, 99999);
    InvoiceNO();
});



var offerapplied = 1;
var FirstName;
var LastName;
var Email;
var Address;
var Phone;
var City;
var PostCode;
var Country;
var State;
var PaymentType;

var SDAFirstName;
var SDALastName;
var SDAEmail;
var SDAAddress;
var SDAPhone;
var SDACity;
var SDAPostCode;
var SDACountry;
var SDAState;




function GoToCheckOut() {

    $.ajax({
        type: "POST",
        url: "productdetailHandler.asmx/GoToCheckOut",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);


            var Arr = obj.listItem;

            var sMainCategory = Arr.ItemList;
            var ul;

            $("#lblSubTotal").text(obj.SubTotal);
            $("#lblVAT").text(obj.VAT);
            $("#lblTotall").text(obj.GrndTotal);
            $("#Amount").val(obj.GrndTotal);

            $("#SubTotal").val(obj.SubTotal);

            var url = '';
            $("#ProductCart").empty();

            for (var i = 0; i < sMainCategory.length; i++) {

                var UnitTotal = sMainCategory[i].price * sMainCategory[i].quantity;


                url = "Admin/ProductImages/" + sMainCategory[i].spltImage;

                var Quantity = sMainCategory[i].quantity;
                $("#Quantity").text(Quantity);

                ul += '<tr class="cart-item first last">'
                ul += '<td class="product-item first" data-title="Items">'
                ul += '<a class="image" href="#"><img alt="' + sMainCategory[i].productName + '" src="' + url + '"></a>'
                ul += '<div class="product-item-details"><span class="cart-title"><a href="#" title="">' + sMainCategory[i].productName + '</a></span> '
                ul += '</div>'
                ul += '</td>'
                ul += '<td class="price" data-title="Price"><span class="money">' + sMainCategory[i].price + '</span></td>'
                ul += '<td class="quantity" data-title="Quantity" id="oldQuant' + i + '"><input type="text" name="updates[]" value="' + sMainCategory[i].quantity + '" onclick="changequantityBlock(' + i + ')"></td>'

                ul += '<td class="quantity" data-title="Quantity" id="updateQuan' + i + '" style="display:none ">'
                ul += '<select class="select" id="updateQuanSelect' + i + '"   onchange="UpdateQuantity(' + sMainCategory[i].productID + ',' + i + ')" style="color:black;border-color: #7dcbb6;margin-top: 33px;height: 25px;">'
                ul += ' <option value="0" >Select New Quantity</option>'
                ul += ' <option value="1" >1</option>'
                ul += ' <option value="2" >2</option>'
                ul += ' <option value="3" >3</option>'
                ul += ' <option value="4" >4</option>'
                ul += ' <option value="5" >5</option>'
                ul += ' <option value="6" >6</option>'
                ul += ' <option value="7" >7</option>'
                ul += ' <option value="8" >8</option>'
                ul += ' <option value="9" >9</option>'
                ul += ' <option value="10" >10</option>'
                ul += '</select>'

                ul += '<td class="total" data-title="Total Price"><span class="money">' + UnitTotal + '</span></td>'
                ul += '<td class="total last" data-title="Remove Item"><a href="#" class="remove" onclick="RemoveItem(' + sMainCategory[i].productID + ')"><i class="fa fa-times" aria-hidden="true"></i></a></td>'
                ul += '</tr>'

            }
            ul += '</tbody>'
            ul += '</table>'

            var s = ul.split('undefined')
            ProductName = s[1];
            $("#ProductCart").append(ProductName);


        },
        error: function () {
        }

    });
}

function changequantityBlock(id) {
    $("#updateQuan" + id).css("display", "block");
    $("#oldQuant" + id).css("display", "none");

}
function UpdateQuantity(Product_nid, i) {
    var newquantity = $("#updateQuanSelect" + i).val();
    var Data = { Pro_ID: Product_nid, newquantity: newquantity }
    var Jason = JSON.stringify(Data);
    $.ajax({
        type: "POST",
        url: "productdetailHandler.asmx/UpdateQuantity",
        data: Jason,
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var newtotal = result.GrndTotal;
            //$("#Newprice").val(newtotal);
            //$("#oldprice").css("display", "none");
            //$("#Newprice").css("display", "block");
            // window.location.reload();
            GoToCheckOut();
        }
    });

}







function Check() {
    if (Addresscheckbox.checked) {
        Copy();
    }
    else {
        UnCopy();
    }
}

function Copy() {
    bValid = Validatation2();
    if (bValid) {
        $('#SDA_inputFirstName').val(FirstName);
        $('#SDA_inputLastName').val(LastName);
        $('#SDA_inputEMail').val(Email);
        $('#SDA_inputAdress').val(Address);
        $('#SDA_inputPhone').val(Phone);
        $('#SDA_inputCity').val(City);
        $('#SDA_inputPostCode').val(PostCode);
        $('#SDA_inputCountry').val(Country);
        $('#SDA_inputState').val(State);
    }

}

function UnCopy() {
    $('#SDA_inputFirstName').val(' ');
    $('#SDA_inputLastName').val(' ');
    $('#SDA_inputEMail').val(' ');
    $('#SDA_inputAdress').val(' ');
    $('#SDA_inputPhone').val(' ');
    $('#SDA_inputCity').val(0);
    $('#SDA_inputPostCode').val(' ');
    $('#SDA_inputCountry').val(0);
    $('#SDA_inputState').val(0);

}

function AddPopup() {
    debugger;

    // $('#btn_AddLocation1').val("Save");
    $("#AddNewLocation").modal("show");
    //$("#txt_Location").val('');
    //$("#txt_Latitude").val('');
    //$("#txt_Longitude").val('');

}



function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function InvoiceNO() {
    rnd = 'INVC-' + RandomNo;
    // alert(rnd);
    $("#lblInvoiceNo").val(rnd);
    $("#order_id").val(rnd);
}
function Validatation() {
    var reg = new RegExp('[0-9]$');
    FirstName = $('#inputFirstName').val();
    LastName = $('#inputLastName').val();
    Email = $('#inputEMail').val();
    Address = $('#inputAdress').val();
    Phone = $('#inputPhone').val();
    City = $('#inputCity').val();
    PostCode = $('#inputPostCode').val();
    Country = $('#inputCountry').val();
    State = $('#inputState').val();



    if (FirstName == "") {
        alert('Please Enter FirstName');
        return false;
    }

    if (LastName == "") {
        alert('Please Enter LastName');
        return false;
    }

    if (Email == "") {
        alert('Please Enter Email');
        return false;
    }
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if (!emailReg.test($("#inputEMail").val())) {
        $("#inputEMail").focus();
        alert("Please Enter valid Email ID");
        return false;
    }
  
    if (Address == "") {
        alert('Please Enter Address');
        return false;
    }
    if (Country == "" || Country == "select Country" || Country == "0" || Country == null) {
        alert('Please Enter Country');
        return false;
    }
    if (State == "" || State == "select State" || State == "0" || State == null) {
        alert('Please Enter State');
        return false;
    }
    if (City == "" || City == "select city" || City == "0" || City == null) {
        alert('Please Enter City');
        return false;
    }
    if (PostCode == "") {
        alert('Please Enter PostCode address');
        return false;
    }
    else {
        if (!(reg.test(PostCode))) {
            bValid = false;
            alert(" Post Code. must be numeric.");
            return;
        }
    }

    if (Phone == "") {
        bValid = false;
        alert("Please Enter Mobile No");
        return;
    }
    else {
        if (!(reg.test(Phone))) {
            bValid = false;
            alert("* Mobile no. must be numeric.");
            return;
        }
    }



    return true;
}


function Apply() {
    var Type = $("#txt_Offer").val();
    if (Type == "") {
        alert("Please Type Offer Code");
        return false;
    } else if (Type != "YEND30") {
        alert("Please Type Valid Offer Code !!!");
        return false;
    }
    var OfferType = Type.toUpperCase();
    var Offer = "YEND30";
  
    var Offerval = Offer.toUpperCase();
    //var Offer15val = Offer15.toUpperCase();
    //var OfferXmasval = OfferXmas.toUpperCase();
    var TotalAmount = $("#Amount").text();
    var Total= TotalAmount.split('.');
    var TotalCost = Total[1];
    if (OfferType == Offerval) {
        var OfferValue = parseFloat(TotalCost) * (30 / 100);

        var TotalPrice = parseFloat(TotalCost) - parseFloat(OfferValue);

        $("#Chek").attr("href", "checkout.html?10");

        $("#SubTotal").text('Rs.'+TotalPrice);
        $("#Amount").text('Rs.'+TotalPrice);
        $("#txt_Offer").val('');
        $("#btn_Offer").prop("disabled", true);
        $("#tt-dropdown-menu").prop("disabled", true);
        var data = {
            OfferValue:OfferValue,
            TotalPrice: TotalPrice

        }
        $.ajax({
            type: "POST",
            url: "webservices/General.asmx/GetCouponPrice",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;


                Arr = result.Arr;
                if (result.retCode == 1) {
                    alert("Offer Applied, Amount Reduced.");
                }
                else {
                    alert("Offer Code Not Matched.")
                    $("#txt_Offer").val('');
                }
            }
        });
      
    }
   
}



function ApplyCouponCode() {

    if ($("#txt_Offer").val() == "") {
        alert("Please Type Offer Code");
        return false;
    }

    var CouponCode = $("#txt_Offer").val();

    var Total = $("#lblTotall").text();
    //var OfferType = Type.toUpperCase();
    var data = { Code: CouponCode, Total: Total }
    $.ajax({
        type: "POST",
        url: "productdetailHandler.asmx/LoadAllCouponForMap",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;


            Arr = result.Arr;
            if (result.retCode == 1) {

                New_totalList = result.New_totalList;
                $("#txt_Offer").val('');
                $("#lblTotall").empty();
                alert("Offer is applied.");
                $("#lblTotall").append(New_totalList);
                $("#Amount").val(New_totalList);
                $("#OfferTotal").val(New_totalList);

                for (i = 0; i < Arr.length; i++) {
                    offerPercent = Arr[i].Percent;
                    offer_nID = Arr[i].CouponID;
                }

                $("#OfferId").val(offer_nID);
                $("#offerPercent").val(offerPercent);
                $("#txt_Offer").attr("disabled", "disabled");
                $("#btn_Offer").attr("disabled", "disabled");
            }
            else {
                alert("Offer Code Not Matched.")
                $("#txt_Offer").val('');
            }
        }
    });
}