﻿
$(document).ready(function () {
    pageURL = $(location).attr("href");
    GetUserData();
});

var userdata = "";
var pageURL = "";
function GetUserData() {
    $.ajax({
        type: "POST",
        url: "webservices/General.asmx/GetUserData",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = JSON.parse(response.d);
            userdata = result;
            console.log(userdata);
            Userhtml();
        },
    });
}

function ProceedToCheckout() {
    if (userdata != "") {
        if (userdata.retCode == 1) {
            window.location.href = "checkout.html"
        }
        else {
            window.location.href = "login.html?pre=current";
          //  $(location).attr('href', 'login.html?' + btoa('pre=current'));
        }
    }
    else {
        window.location.href = "login.html?pre=current";
       // $(location).attr('href', 'login.html?' + btoa('pre=current'));
    }
}


function MyOrder() {
    if (userdata != "") {
        if (userdata.retCode == 1) {
            window.location.href = "my_orders.html";
        } else
            window.location.href = "login.html?pre=my_orders";
       // $(location).attr('href', 'login.html?' + btoa('pre=my_orders'));
    }
    else {
        window.location.href = "login.html?pre=my_orders"
       // $(location).attr('href', 'login.html?' + btoa('pre=my_orders'));
    }

}
function logout() {
    $.ajax({
        type: "Post",
        url: "webservices/General.asmx/logout",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var response = JSON.parse(response.d);
            if (response.retCode == 1) {
                GetUserData();
                // alert("User Logout Successfully!!!");
            }
            console.log(response);
        }
    })
}


function Userhtml() {
    if (userdata != "") {
        $('.user').empty();
        if (userdata.retCode == 1) {
            $('.login_register').hide();
            $('.signout').show();
            $('.user').html("<a href='JavaScript:void(0);'><i class='icon-f-68'></i>welcome.. " + userdata.userdetails.Email + "</a>")
        }
        else {
            $('.login_register').show();
            $('.signout').hide();
        }
    }
    else {
        $('.user').empty();
        $('.login_register').show();
        $('.signout').hide();
    }
}

