﻿
function carthtml(items, total) {
    var products = items.ItemList;
    $(".caritem-empty").hide();
    $(".cartitem").show();
    $("#cart-items").empty();
    var html = ""
    for (var i = 0; i < products.length; i++) {
        html += '<tr>                                                                                                                          '
        html += '<td>                                                                                          '
        html += '    <div class="tt-product-img">                                                              '
        html += '        <img src="' + products[i].imageUrl + '" data-src="' + products[i].imageUrl + '" alt="">'
        html += '    </div>                                                                                    '
        html += '</td>                                                                                         '
        html += '<td>                                                                                          '
        html += '    <h2 class="tt-title">                                                                     '
        html += '        <a style="cursor:pointer;">' + products[i].productName + '</a>                                                    '
        html += '    </h2>                                                                                     '
        //html += '    <ul class="tt-list-description">                                                          '
        //html += '        <li>Size: 22</li>                                                                     '
        //html += '        <li>Color: Green</li>                                                                 '
        //html += '    </ul>                                                                                     '
        html += '    <ul class="tt-list-parameters">                                                           '
        html += '        <li>                                                                                  '
        html += '            <div class="tt-price">                                                            '
        html += '               Rs. ' + products[i].price + '                                                                          '
        html += '            </div>                                                                            '
        html += '        </li>                                                                                 '
        html += '        <li>                                                                                  '
        html += '            <div class="detach-quantity-mobile"></div>    <h2 class="tt-title">  Quantity : ' + products[i].quantity + ' </h2>                                    '
        html += '        </li>                                                                                 '
        html += '        <li>                                                                                  '
        html += '            <div class="tt-price subtotal">                                                   '
        html += '               Rs. ' + parseInt(products[i].quantity * products[i].price) + '                                                                         '
        html += '            </div>                                                                            '
        html += '        </li>                                                                                 '
        html += '    </ul>                                                                                     '
        html += '</td>                                                                                         '
        html += '<td>                                                                                          '
        html += '    <div class="tt-price">                                                                    '
        html += '        Rs.' + products[i].price + '                                                                                  '
        html += '    </div>                                                                                    '
        html += '</td>                                                                                         '
        html += '<td>  <h2 class="tt-title">  Quantity : ' + products[i].quantity + ' </h2>                                                                                       '
        //html += '    <div class="detach-quantity-desctope">                                                    '
        //html += '        <div class="tt-input-counter style-01">                                               '
        //html += '            <span class="minus-btn"></span>                                                   '
        //html += '            <input type="text" value="' + products[i].quantity + '" size="20">                                            '
        //html += '            <span class="plus-btn"></span>                                                    '
        //html += '        </div>                                                                                '
        //html += '    </div>                                                                                    '
        html += '</td>                                                                                         '
        html += '<td>                                                                                          '
        html += '    <div class="tt-price subtotal">                                                           '
        html += '       Rs. ' + parseInt(products[i].quantity * products[i].price) + '                                                                                 '
        html += '    </div>                                                                                    '
        html += '</td>                                                                                         '
        html += '<td>                                                                                          '
        html += '    <a style="cursor:pointer;" onclick="RemoveItem(\'' + products[i].productCode + '\');" class="tt-btn-close"></a>'
        html += '</td>                                                                                         '
        html += '</tr>                                                                                             '
    }
    $("#cart-items").append(html);
    $(".sub").text('Rs.' +total);
    $(".total").text('Rs.' +total);
    InputCounter();
}

function emptycart() {
    $(".caritem-empty").show();
    $(".cartitem").hide();
}

function InputCounter() {
    var qountity = 0;
    $(".tt-input-counter").find('.minus-btn, .plus-btn').on('click', function (e) {
        var $input = $(this).parent().find('input');
        var count = parseInt($input.val(), 10) + parseInt(e.currentTarget.className === 'plus-btn' ? 1 : -1, 10);
        $input.val(count).change();
        qountity = count;
    });
    $(".tt-input-counter").find("input").change(function () {
        var _ = $(this);
        var min = 1;
        var val = parseInt(_.val(), 10);
        var max = parseInt(_.attr('size'), 10);
        val = Math.min(val, max);
        val = Math.max(val, min);
        _.val(val);
        qountity = val;
    })
    .on("keypress", function (e) {
        if (e.keyCode === 13) {
            e.preventDefault();
        }
    });
};