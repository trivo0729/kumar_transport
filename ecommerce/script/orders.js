﻿$(document).ready(function () {
    getorders();
});

function getorders() {
    $.ajax({
        type: "POST",
        url: "webservices/General.asmx/getorders",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = JSON.parse(response.d);
            if (result.retCode == 1) {
                Orderhtml(result.Orders);
                console.log(result.Orders)
            }
            else {
            }
        },
    });

}

function Orderhtml(Orders) {
    $("#orders").empty();
    var html = "";
    for (var i = 0; i < Orders.length; i++) {
        html += '<tr style="border:1px inset">'
        html += '<td style="width:10%;text-align: center;border:1px inset">' + Orders[i].InvoiceNo + '</td>'
        html += '<td style="width:10%;text-align: center;border:1px inset">' + Orders[i].InvoiceDate + '</td>'
        html += '<td style="width:58%;border:1px inset">'
        html += '<table style="width:100%">'
        html += '<tr>'
        html += '<th style="width:70%;border-right:1px inset;border-bottom:1px inset">Product Name</th>'
        html += '<th style="width:10%;border-right:1px inset;;border-bottom:1px inset;text-align: center;">Quantity</th>'
        html += '<th style="width:20%;;border-bottom:1px inset;text-align: center;">Price</th>'
        html += '</tr>'
        for (var j = 0; j < Orders[i].Products.length; j++) {
            if (j != Orders[i].Products.length-1) {
                html += '<tr>'
                html += '<td style="border-right:1px inset;border-bottom:1px inset;">' + Orders[i].Products[j].Name + '</td>'
                html += '<td style="border-right:1px inset;text-align: center;border-bottom:1px inset;">' + Orders[i].Products[j].Quantity + '</td>'
                html += '<td style="text-align: center;border-bottom:1px inset;">' + Orders[i].Products[j].Price + '</td>'
                html += '</tr>'
            }
            else {
                html += '<tr>'
                html += '<td style="border-right:1px inset;">' + Orders[i].Products[j].Name + '</td>'
                html += '<td style="border-right:1px inset;text-align: center;">' + Orders[i].Products[j].Quantity + '</td>'
                html += '<td style="text-align: center;">' + Orders[i].Products[j].Price + '</td>'
                html += '</tr>'
            }
            
        }
        html += '</table>'
        html += '</td>'
        html += '<td style="width:12%;text-align: center;border:1px inset">Rs. ' + Orders[i].Grand_Total + '</td>'
        html += '<td style="width:10%;text-align: center;border:1px inset">' + Orders[i].Status + '</td>'
        html += '</tr>'
    }
    $("#orders").html(html);
}